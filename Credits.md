# Credits for XcInspector

## Icon

Our Icon graphic is based on the Apple Developer application icon, and incorporates an image of a canary taken from: [Canario silvestre, Serinus canarius(♂)](https://www.flickr.com/photos/22009023@N03/6818708977) by [Juan Emilio](https://www.flickr.com/photos/juan_e/). Used under [Creative Commons CC-BY-SA License](https://creativecommons.org/licenses/by-sa/2.0/).

## Software Libraries & Packages

Numerous software libraries and packages from Apple Computer, Inc., are used by this application.

### Third Party Libraries & Packages

* [ViewInspector](https://github.com/nalexn/ViewInspector). Used under MIT License.
