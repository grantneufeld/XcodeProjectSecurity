// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

/// A recognized type of resource (file or directory) that can be processed by this app.
public protocol Target {
    /// Where to access the resource target (typically a `FileAccessor` or `DirectoryAccessor`).
    var accessor: Accessor { get set }

    /// Perform a scan of the target, identifying any rule violations.
    /// - Returns: A `Report` containing any rule violations found.
    func scan() throws -> Report
}
