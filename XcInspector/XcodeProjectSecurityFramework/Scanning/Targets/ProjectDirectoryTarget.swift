// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

/// A directory for an Xcode project (contains a `.xcodeproj` directory).
public struct ProjectDirectoryTarget: Target {
    /// Where to access the resource target (should always be a `DirectoryAccessor`).
    public var accessor: Accessor

    public init(accessor: Accessor) {
        self.accessor = accessor
    }

    /// Perform a scan of the target, identifying any rule violations.
    /// - Returns: A `Report` containing any rule violations found.
    public func scan() throws -> Report {
        guard let dirAccessor = self.accessor as? DirectoryAccessor else {
            throw AccessorError.notADirectory
        }
        let rule = ExecutableFilesRule()
        let report = try rule.scan(target: dirAccessor)
        for resource in dirAccessor {
            if let target = resource.target {
                report += try target.scan()
            }
        }
        return report
    }
}
