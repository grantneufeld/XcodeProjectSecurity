// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

/// A `project.pbxproj` file.
public struct PBXProjTarget: Target {
    /// Where to access the resource target (should always be a `FileAccessor`).
    public var accessor: Accessor

    public init(accessor: Accessor) {
        self.accessor = accessor
    }

    /// Perform a scan of the target, identifying any rule violations.
    /// - Returns: A `Report` containing any rule violations found.
    public func scan() throws -> Report {
        guard let fileAccessor = self.accessor as? FileAccessor else {
            throw AccessorError.notAFile
        }
        let sandboxRule = UserScriptSandboxingRule()
        let report = try sandboxRule.scan(target: fileAccessor)
        let rule = EmbeddedScriptsRule()
        report += try rule.scan(target: fileAccessor)
        return report
    }
}
