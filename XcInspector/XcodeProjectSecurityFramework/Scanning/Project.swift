// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

/// Tracks a scan report for a given project (defined by its `Accessor`).
@Observable
public final class Project: Equatable, Hashable, Identifiable {
    /// The file or directory defining the project that was scanned.
    public var accessor: Accessor
    /// The report from scanning the project.
    public var report: Report?

    // MARK: - Identifiable

    /// Unique identifier for the project.
    /// (Uses the URL of the file or directory for the project.)
    public var id: URL {
        self.accessor.url
    }

    // MARK: - Equatable

    public static func == (lhs: Project, rhs: Project) -> Bool {
        lhs.accessor.fullPath == rhs.accessor.fullPath &&
        lhs.report == rhs.report
    }

    // MARK: - Hashable

    /// Adds the values of this project into the given hasher.
    /// - Parameter hasher: The hash function.
    public func hash(into hasher: inout Hasher) {
        hasher.combine(accessor.fullPath)
    }

    /// - Parameters:
    ///   - accessor: The file or directory for a project.
    ///   - report: A report generated by the scanner.
    public init(accessor: Accessor, report: Report? = nil) {
        self.accessor = accessor
        self.report = report
    }
}
