// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation

/// Enables Report to function as a Sequence of rule violations (`for violation in report` type calls).
public final class ReportIterator: IteratorProtocol {
    private var collection: Report
    private var index: Int = 0
    private var max: Int

    public init(_ collection: Report) {
        self.collection = collection
        self.max = collection.count
    }

    public func next() -> RuleViolation? {
        guard self.index < self.max else {
            return nil
        }
        let result = self.collection[index]
        self.index += 1
        return result
    }
}

/// Collection of Rule Violations.
public final class Report: Equatable, ObservableObject, RandomAccessCollection, Sequence {
    /// The collection of rule violations.
    @Published private var violations: [RuleViolation] = []

    /// The date & time when the report was generated.
    public var timestamp = Date(timeIntervalSince1970: 0)

    /// A Boolean value indicating whether the report is empty.
    public var isEmpty: Bool {
        self.violations.isEmpty
    }

    /// The position of the first violation in a nonempty collection.
    public var startIndex: Int { self.violations.startIndex }
    /// The collection’s “past the end” position—that is,
    /// the position one greater than the last valid subscript argument.
    public var endIndex: Int { self.violations.endIndex }

    /// The number of violations in the report.
    public var count: Int {
        self.violations.count
    }

    public static func == (lhs: Report, rhs: Report) -> Bool {
        lhs.violations == rhs.violations
    }

    /// - Parameters:
    ///   - lhs: The destination Report that will have violations (from `rhs`) added to it.
    ///   - rhs: The source of violations to append to the destination (`lhs`)
    public static func += (lhs: Report, rhs: Report) {
        lhs.violations += rhs.violations
    }

    public init() {}

    /// - Parameter index: The zero-offset index of the violation to access from the report.
    public subscript(_ index: Int) -> RuleViolation {
        self.violations[index]
    }

    /// Adds a new violation at the end of the report.
    /// - Parameter violation: The `RuleViolation` to add to the report
    public func append(_ violation: RuleViolation) {
        self.violations.append(violation)
    }

    // MARK: - Sequence

    /// Get an iterator used to enable the Sequence behaviour (`for x in collection`).
    /// - Returns: A `ReportIterator` that will start with the first violation in the collection.
    public func makeIterator() -> ReportIterator {
        ReportIterator(self)
    }

    // MARK: - Reporting

    /// Returns all of the violations in the report as plain text.
    ///
    /// There is a linebreak (`\n`) between each violation.
    /// - Parameter includeDetails: If `true`, include details with violations that have them.
    ///   e.g., an embedded script violation will include the text of the script.
    /// - Returns: The report of rule violations as plain text.
    public func plainText(includeDetails: Bool = false) -> String {
        var text = ""
        for violation in self.violations {
            text.append("\(violation.plainText())\n")
            if includeDetails, let details = violation.data?.decodeSlashEncodedCharacters {
                let lines = details.split(separator: /[\n\r\r\n]/, omittingEmptySubsequences: false)
                let indentedLines = lines.map { "    \($0)" }
                text.append(indentedLines.joined(separator: "\n"))
                text.append("\n")
            }
        }
        return text
    }

    /// Get a line of text describing the statistics for the report (i.e., how many violations found).
    /// - Returns: The report statistics as a line of text.
    public func plainTextStats() -> String {
        String(
            AttributedString(
                localized: "Found ^[\(self.violations.count) warning](inflect: true)."
            ).characters
        )
    }

    /// Save the report as (Xcode compatible) plain text.
    /// - Parameter accessor: The destination file to save the report to. Will be overwritten.
    /// - Throws: If the file cannot be written to.
    public func save(_ accessor: FileAccessor) throws {
        try accessor.overwrite(self.plainText())
    }
}
