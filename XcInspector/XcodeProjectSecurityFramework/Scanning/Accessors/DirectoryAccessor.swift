// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation

///// Enables DirectoryAccessor to function as a Sequence of Accessors (`for accessor in directory` type calls).
public final class DirectoryAccessorIterator: IteratorProtocol {
    private var collection: DirectoryAccessor
    private var enumerator: FileManager.DirectoryEnumerator?

    public init(_ collection: DirectoryAccessor) {
        self.collection = collection
        self.enumerator = FileManager.default.enumerator(
            at: collection.url, includingPropertiesForKeys: nil, options: [.skipsSubdirectoryDescendants]
        )
        enumerator?.skipDescendants()
    }

    public func next() -> Accessor? {
        if let itemURL = self.enumerator?.nextObject() as? URL {
            if itemURL.hasDirectoryPath {
                return DirectoryAccessor(url: itemURL)
            }
            return FileAccessor(url: itemURL)
        }
        return nil
    }
}

/// An accessor for a directory at a specified path.
public final class DirectoryAccessor: Accessor, Equatable, Sequence {
    /// The last report, if any, generated from scanning the directory.
    public var report: Report?
    /// The path to the directory. Must be complete path.
    public var fullPath: String { self._url.relativePath }

    /// The complete `URL` for the directory.
    public var url: URL { self._url }
    /// Get the full name of the directory.
    public var filename: String { self.url.lastPathComponent }

    /// Returns `true` if the directory exists.
    public var exists: Bool {
        FileManager.default.fileExists(atPath: fullPath)
    }

    /// The number of elements in the directory.
    public var count: Int {
        FileManager.default.subpaths(atPath: self.fullPath)?.count ?? 0
    }

    private var _url: URL

    public init(url: URL) {
        self._url = url
    }

    public static func == (lhs: DirectoryAccessor, rhs: DirectoryAccessor) -> Bool {
        lhs.fullPath == rhs.fullPath
    }

    /// Make sure that the specified directory exists,
    /// creating it and any missing parent directories if necessary
    public func ensureExists() throws {
        guard !self.exists else {
            return
        }
        try FileManager.default.createDirectory(at: self.url, withIntermediateDirectories: true)
    }

    /// Try to delete the directory.
    public func delete() throws {
        try FileManager.default.removeItem(at: self.url)
    }

    // MARK: - Sequence

    /// Get an iterator used to enable the Sequence behaviour (`for x in collection`).
    /// - Returns: A `DirectoryAccessorIterator` that will start with the first Server in the collection.
    public func makeIterator() -> DirectoryAccessorIterator {
        DirectoryAccessorIterator(self)
    }

    // MARK: - Target

    /// The filename extension for a `.xcodeproj` file. (which is actually a macOS package directory)
    let xcodeProjExtension = ".xcodeproj"
    /// Determine what sort of `Target` (if any) the accessor represents.
    public var target: Target? {
        guard self.exists else {
            return nil
        }
        if self.filename.hasSuffix(self.xcodeProjExtension) {
            return XcodeProjTarget(accessor: self)
        }
        for resource in self {
            if resource.filename.hasSuffix(self.xcodeProjExtension) {
                return ProjectDirectoryTarget(accessor: self)
            }
        }
        return nil
    }
}
