// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation

public enum FileAccessorError: Error {
case failedToConvertTextToData
}

/// A wrapper around reading and writing to files.
public final class FileAccessor: Accessor, Equatable {
    /// The last report, if any, generated from scanning the file.
    public var report: Report?
    /// The full path of file.
    public var fullPath: String { self._url.relativePath }

    /// The complete `URL` for the file.
    public var url: URL { self._url }
    private var _url: URL
    /// Get the full name of the file.
    public var filename: String { self.url.lastPathComponent }
    /// Indicates a file exists at a specified path.
    public var exists: Bool {
        FileManager.default.fileExists(atPath: fullPath)
    }

    /// - Returns: `true` if the file is executable or has a posix executable bit set.
    public var isExecutable: Bool {
        // FileManager will return false on an executable file when sandboxed
        // without the app having permission to execute.
        FileManager.default.isExecutableFile(atPath: self.fullPath) || self.hasExecutableBit
    }

    /// - Returns: `true` if a posix executable bit is set on the file.
    public var hasExecutableBit: Bool {
        do {
            let attributes = try FileManager.default.attributesOfItem(atPath: self.fullPath)
            let permissions = attributes[.posixPermissions]
            if let permissionsNumber = permissions as? Int {
                return permissionsNumber & 0o111 != 0
            }
        } catch {}
        return false
    }

    /// - Parameter url: The `URL` for a file on the local filesystem.
    public init(url: URL) {
        self._url = url
    }

    // MARK: - Equatable

    public static func == (lhs: FileAccessor, rhs: FileAccessor) -> Bool {
        lhs.fullPath == rhs.fullPath
    }

    // MARK: - File Data

    /// Read the entire contents of the file as text.
    /// - Returns: The file contents as a text `String`.
    public func read() throws -> String {
        try String(contentsOf: self.url, encoding: .utf8)
    }

    /// If the file exists, replace its content with the supplied text.
    /// Otherwise, create the file with the supplied text,
    /// - Parameter text: The text to write to the file.
    public func overwrite(_ text: String) throws {
        let textData = try self.textAsData(text)
        if let fileHandle = FileHandle(forWritingAtPath: self.url.path) {
            defer { fileHandle.closeFile() }
            try fileHandle.write(contentsOf: textData)
            try fileHandle.truncate(atOffset: try fileHandle.offset())
        } else {
            try textData.write(to: self.url, options: .atomic)
        }
    }

    /// Convert a text string to a UTF8 `Data` structure.
    /// - Parameter text: The text `String`.
    /// - Returns: A UTF8 `Data` structure of the text.
    private func textAsData(_ text: String) throws -> Data {
        guard let textData = text.data(using: .utf8) else {
            throw FileAccessorError.failedToConvertTextToData
        }
        return textData
    }

    /// Removes the file at the specified URL.
    public func delete() throws {
        try FileManager.default.removeItem(at: self.url)
    }

    /// The filename extension for a `.pbxproj` file.
    let pbxprojExtension = ".pbxproj"
    /// Determine what sort of `Target` (if any) the accessor represents.
    public var target: Target? {
        if self.filename.hasSuffix(self.pbxprojExtension) {
            return PBXProjTarget(accessor: self)
        }
        return nil
    }
}
