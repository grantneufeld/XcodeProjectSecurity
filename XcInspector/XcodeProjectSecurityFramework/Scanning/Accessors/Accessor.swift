// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation

public enum AccessorError: Error {
case invalidURL
case notADirectory
case notAFile
}

/// Defines accessing a given resource.
public protocol Accessor {
    /// The full path of the resource.
    var fullPath: String { get }
    /// The URL of the resource.
    var url: URL { get }
    /// Get the full name of the resource.
    var filename: String { get }
    /// Indicates the resource exists at its specified path.
    var exists: Bool { get }
    /// The last report, if any, generated from scanning the accessor.
    var report: Report? { get set }

    /// Removes the resource specified by the accessor.
    func delete() throws

    /// Determine what sort of `Target` (if any) the accessor represents.
    var target: Target? { get }
}
