// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation

/// Determine which type of Accessor is needed for a given URL.
public enum AccessorHelper {
    /// Determine which type of Accessor is needed for a given URL.
    /// - Parameter url: A `URL` for a resource on the local file system.
    /// - Returns: A `DirectoryAccessor` if the url points to a directory.
    ///   Otherwise, a `FileAccessor`.
    /// - Throws: `AccessorError.invalidURL` if the url does not point to a file or directory.
    public static func accessor(for url: URL) throws -> Accessor {
        guard url.isFileURL else {
            throw AccessorError.invalidURL
        }
        if url.hasDirectoryPath {
            return DirectoryAccessor(url: url)
        }
        return FileAccessor(url: url)
    }
}
