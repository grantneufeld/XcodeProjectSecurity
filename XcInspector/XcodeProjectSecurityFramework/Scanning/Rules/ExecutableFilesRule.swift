// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation

/// A rule to check for the presence of executable files in a directory.
public struct ExecutableFilesRule: Rule {
    /// Human-readable description for a violation of the rule.
    public var message: String { "Found an executable file in the project directory." }

    public init() {}

    /// Scan (check) a directory, and its sub-directories, for executable files.
    /// - Parameter target: A `DirectoryAccessor`.
    /// - Returns: A collection containing a `RuleViolation` for each executable file found.
    public func scan(target: Accessor) throws -> Report {
        guard let target = target as? DirectoryAccessor else {
            throw RuleError.targetDirectoryIsNotDirectory
        }
        return try self.scanDirectory(target)
    }

    /// Recursive function to scan a directory, and its sub-directories, for executable files.
    /// - Parameter directory: The directory to scan.
    /// - Returns: A `RuleViolation` collection.
    private func scanDirectory(_ directory: DirectoryAccessor) throws -> Report {
        let report = Report()
        for resource in directory {
            if let file = resource as? FileAccessor {
                if file.isExecutable {
                    report.append(self.violation(file))
                }
            } else {
                report += try scanDirectory(resource as! DirectoryAccessor)
            }
        }
        return report
    }

    /// Return a violation for the given file.
    /// - Parameter file: The violating file.
    /// - Returns: A `RuleViolation`.
    private func violation(_ file: FileAccessor) -> RuleViolation {
        RuleViolation(rule: self, file: file.fullPath)
    }

    /// Hack to be equivalent of conformance to the `Equatable` protocol.
    /// (Because putting `Equatable` protocol on a protocol, such as `Rule`, makes things “messy”.)
    /// - Parameter other: A `Rule` to compare this rule with.
    /// - Returns: `true` if the given rule is also an `ExecutableFilesRule`.
    public func equals(_ other: Rule) -> Bool {
        (other as? Self) != nil
    }
}
