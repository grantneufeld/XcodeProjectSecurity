// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

/// A rule to check that a .pbxproj file has the user script sandboxing flag on.
public struct UserScriptSandboxingRule: Rule, Equatable {
    /// Human-readable description for a violation of the rule.
    public var message: String { "User Script Sandboxing flag not set in project." }

    public init() {}

    /// Scan (check) a `.pbxproj` file for the user script sandboxing flag.
    /// - Parameter target: A `FileAccessor` for a `.pbxproj` file.
    /// - Returns: A collection containing a `RuleViolation`if the user script sandboing flag is missing, or off.
    public func scan(target: Accessor) throws -> Report {
        guard let target = target as? FileAccessor else {
            throw RuleError.targetFileIsNotFile
        }
        guard target.exists else {
            throw RuleError.targetFileDoesNotExist
        }
        guard target.filename.hasSuffix(".pbxproj") else {
            throw RuleError.targetFileIsNotPbxproj
        }
        let report = Report()
        let data = try target.read()
        var foundFlag = false
        var lineNumber = 0
        for line in data.split(separator: "\n") {
            lineNumber += 1
            if let match = line.firstMatch(of: /ENABLE_USER_SCRIPT_SANDBOXING *= *(YES|NO)/) {
                foundFlag = true
                let yesOrNo = String(match.1)
                if yesOrNo != "YES" {
                    let violation = RuleViolation(
                        rule: self, file: target.fullPath, line: lineNumber
                    )
                    report.append(violation)
                }
            }
        }
        if !foundFlag {
            let violation = RuleViolation(
                rule: self, file: target.fullPath
            )
            report.append(violation)
        }
        return report
    }

    /// Hack to be equivalent of conformance to the `Equatable` protocol.
    /// (Because putting `Equatable` protocol on a protocol, such as `Rule`, makes things “messy”.)
    /// - Parameter other: A `Rule` to compare this rule with.
    /// - Returns: `true` if the given rule is also an `UserScriptSandboxingRule`.
    public func equals(_ other: Rule) -> Bool {
        (other as? Self) != nil
    }
}
