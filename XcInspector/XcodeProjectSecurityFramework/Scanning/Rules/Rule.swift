// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

public enum RuleError: Error {
case targetDirectoryIsNotDirectory
case targetFileDoesNotExist
case targetFileIsNotFile
case targetFileIsNotPbxproj

case textDoesNotMatchOneOfOurRegex
}

/// A “rule” that can be run to check (`scan`) for possible security issues with an Xcode Project.
public protocol Rule {
    /// Human-readable description for a violation of the rule.
    var message: String { get }

    /// Scan (check) a given target file/directory for possible security issues.
    /// - Parameter target: A `Accessor` for a file or directory.
    /// - Returns: A collection of `RuleViolation`s describing every possible security issue identified.
    func scan(target: Accessor) throws -> Report

    /// Hack to be equivalent of conformance to the `Equatable` protocol.
    /// (Because putting `Equatable` protocol on a protocol, such as `Rule`, makes things “messy”.)
    /// - Parameter other: A `Rule` to compare this rule with.
    /// - Returns: `true` if the rule parameter is equal to the rule.
    func equals(_ other: Rule) -> Bool
}
