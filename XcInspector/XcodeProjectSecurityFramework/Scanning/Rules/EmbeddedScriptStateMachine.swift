// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

/// A state machine to handle scanning through a pbxproj file,
/// to determine if there are embedded scripts.
///
/// Usage:
///
///     let stateMachine = EmbeddedScriptStateMachine(target: pbxProjFileAccessor)
///     let report = try stateMachine.scan()
final class EmbeddedScriptStateMachine {
    private let target: FileAccessor
    private var report = Report()
    private var data = ""
    private var dataLineNumber = 0
    private var state = State.reading
    private var scriptFirstLineNumber = 0
    private var scriptTexts: [String] = []

    private enum State {
        case reading
        case multiline
        case script
    }

    /// - Parameter target: A pbxproj file.
    init(target: FileAccessor) {
        self.target = target
    }

    /// Scan (check) a `.pbxproj` file for embedded scripts.
    /// - Returns: A `Report` containing a `RuleViolation` for each embedded script identified.
    func scan() throws -> Report {
        self.report = Report()
        self.dataLineNumber = 0
        self.data = try target.read()
        for line in self.data.split(separator: "\n", omittingEmptySubsequences: false) {
            self.dataLineNumber += 1
            switch self.state {
            case .reading:
                self.handleStatelessLine(line)

            case .multiline:
                try self.handleLineInMultiline(line)

            case .script:
                try self.handleLineInScript(line)
            }
        }
        return self.report
    }

    private func handleLineInScript(_ line: String.SubSequence) throws {
        if line.wholeMatch(of: /^\s*$/) != nil {
            // (just whitespace or blank line)
            scriptTexts.append("")
        } else if line.firstMatch(of: /^\s*\"/) != nil {
            // "
            // (closing quote)
            finishRuleViolation()
        } else if let match = line.wholeMatch(of: /^\s*([^\"]+)\s*$/) {
            // .. more script ...
            self.append(match.1)
        } else if let match = line.firstMatch(of: /^\s*([^\"]+)\s*\"/) {
            // ... more script ... "
            self.append(match.1)
            finishRuleViolation()
        } else {
            throw RuleError.textDoesNotMatchOneOfOurRegex
        }
    }

    private func handleLineInMultiline(_ line: String.SubSequence) throws {
        if line.wholeMatch(of: /^\s*=\s*$/) != nil {
            // =
            // ignore
        } else if line.wholeMatch(of: /^\s*=\s*"\s*$/) != nil {
            // = "
            self.state = .script
        } else if line.wholeMatch(of: /^\s*=\s*"\s*"\s*/) != nil {
            // = " "
            self.state = .reading
        } else if let match = line.firstMatch(of: /^\s*=\s*"\s*(.+)\s*"/) {
            // = " ... "
            self.append(match.1)
            finishRuleViolation()
        } else if let match = line.wholeMatch(of: /^\s*=\s*"\s*(.+)\s*$/) {
            // = " ...
            self.state = .script
            self.append(match.1)
        } else if line.wholeMatch(of: /^\s*\"\s*$/) != nil {
            // "
            self.state = .script
        } else if let match = line.firstMatch(of: /^\s*\"\s*(.+)\s*\"/) {
            // " ... "
            self.append(match.1)
            finishRuleViolation()
        } else if let match = line.wholeMatch(of: /^\s*\"\s*(.+)\s*$/) {
            // " ...
            self.state = .script
            self.append(match.1)
        } else {
            throw RuleError.textDoesNotMatchOneOfOurRegex
        }
    }

    private func handleStatelessLine(_ line: String.SubSequence) {
        if let match = line.firstMatch(of: /shellScript\s*=\s*\"\s*(.+)\s*\"/) {
            self.append(match.1)
            self.scriptFirstLineNumber = self.dataLineNumber
            finishRuleViolation()
        } else if line.contains("shellScript") {
            self.state = .multiline
            self.scriptFirstLineNumber = self.dataLineNumber
            // shellScript
            // shellScript =
            if line.firstMatch(of: /\"\s*$/) != nil {
                // shellScript = "
                self.state = .script
            } else if let match = line.firstMatch(of: /\"\s*(.+)\s*$/) {
                // shellScript = " ...
                self.state = .script
                self.append(match.1)
            }
        }
    }

    private func append(_ matchGroup: Substring) {
        self.scriptTexts.append(String(matchGroup))
    }

    private func finishRuleViolation() {
        let violation = RuleViolation(
            rule: EmbeddedScriptsRule(),
            file: self.target.fullPath,
            line: self.scriptFirstLineNumber,
            extraMessage: nil,
            data: self.scriptTexts.joined(separator: "\n")
        )
        self.report.append(violation)
        self.scriptTexts = []
        self.state = .reading
    }
}
