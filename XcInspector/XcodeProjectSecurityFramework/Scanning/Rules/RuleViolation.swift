// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation

/// A record of a rule violation.
public struct RuleViolation: Equatable, Identifiable {
    /// Unique identifier for the Identifiable protocol.
    public var id = UUID()
    /// The specific rule that was violated.
    public let rule: Rule
    /// The file that triggered the rule violation.
    public let file: String
    /// The line number that the rule violation was encountered at, if applicable.
    public let line: Int?
    /// Any extra information about this particular rule violation.
    public let extraMessage: String?
    /// Any data associated with the rule violation.
    public let data: String?

    public init(rule: Rule, file: String, line: Int? = nil, extraMessage: String? = nil, data: String? = nil) {
        self.rule = rule
        self.file = file
        self.line = line
        self.extraMessage = extraMessage
        self.data = data
    }

    // MARK: - Equatable

    public static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.rule.equals(rhs.rule) &&
        lhs.file == rhs.file &&
        lhs.line == rhs.line &&
        lhs.extraMessage == rhs.extraMessage &&
        lhs.data == rhs.data
    }

    // MARK: - Report

    /// A plain text description of the rule violation.
    ///
    /// Uses Xcode format (`filename:linenumber:type:message`,
    /// where `type` is one of `error`, `warning`, or `note`).
    /// - Returns: The plain text description of the rule violation.
    public func plainText() -> String {
        "\(self.file):\(self.lineNumberText)warning:\(self.rule.message)"
    }

    /// Get the line number, with trailing colon, if there is a line number set. Otherwise, an empty string.
    private var lineNumberText: String {
        if let line = self.line {
            return "\(line):"
        }
        return ""
    }
}
