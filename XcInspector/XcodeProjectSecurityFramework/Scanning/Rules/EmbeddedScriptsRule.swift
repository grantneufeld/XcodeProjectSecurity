// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

/// A rule to check for the presence of embedded scripts
/// (such as part of a “Run Script” “Build Phase”)
/// in a .pbxproj file.
public struct EmbeddedScriptsRule: Rule {
    /// Human-readable description for a violation of the rule.
    public var message: String { "Found an embedded script in the pbxproj file." }

    public init() {}

    /// Scan (check) a `.pbxproj` file for embedded scripts.
    /// - Parameter target: A `FileAccessor` for a `.pbxproj` file.
    /// - Returns: A Report containing a `RuleViolation` for each embedded script identified.
    public func scan(target: Accessor) throws -> Report {
        guard let target = target as? FileAccessor else {
            throw RuleError.targetFileIsNotFile
        }
        guard target.exists else {
            throw RuleError.targetFileDoesNotExist
        }
        guard target.filename.hasSuffix(".pbxproj") else {
            throw RuleError.targetFileIsNotPbxproj
        }
        let stateMachine = EmbeddedScriptStateMachine(target: target)
        return try stateMachine.scan()
    }

    /// Hack to be equivalent of conformance to the `Equatable` protocol.
    /// (Because putting `Equatable` protocol on a protocol, such as `Rule`, makes things “messy”.)
    /// - Parameter other: A `Rule` to compare this rule with.
    /// - Returns: `true` if the given rule is also an `EmbeddedScriptsRule`.
    public func equals(_ other: Rule) -> Bool {
        (other as? Self) != nil
    }
}
