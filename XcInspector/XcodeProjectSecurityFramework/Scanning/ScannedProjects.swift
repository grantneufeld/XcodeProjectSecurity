// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

/// Collection of the projects that have been scanned.
@Observable
public final class ScannedProjects: RandomAccessCollection {
    public typealias Element = Project

    /// The collection of projects.
    private var collection: [Element] = []

    /// The number of elements in the collection.
    public var count: Int { self.collection.count }
    /// The position of the first element in a nonempty collection.
    ///
    /// For an instance of collection, `startIndex` is always zero.
    /// If the collection is empty, `startIndex` is equal to `endIndex`.
    public var startIndex: Int { self.collection.startIndex }
    /// The collection’s “past the end” position—that is,
    /// the position one greater than the last valid subscript argument.
    public var endIndex: Int { self.collection.endIndex }

    /// Default initializer.
    public init() {}

    /// Adds a new element at the end of the collection.
    /// - Parameter element: The element to append to the collection.
    public func append(_ element: Element) {
        self.collection.append(element)
    }

    /// Returns the position immediately after the given index.
    /// - Parameter index: A valid index of the collection. It must be less than `endIndex`.
    /// - Returns: The index immediately after the given index.
    public func index(after index: Int) -> Int { self.collection.index(after: index) }

    public subscript(_ index: Int) -> Element {
        self.collection[index]
    }
}
