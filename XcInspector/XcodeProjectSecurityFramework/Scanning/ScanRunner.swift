// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation

public enum ScanRunnerError: Error {
    case accessorDoesNotExist
    case unrecognizedAccessor
}

/// The central controller for running a scan/inspection.
///
/// You give the runner an `Accessor` for one of:
///
/// - Xcode project directory (contains a `.xcodeproj` file)
/// - `.xcodeproj` directory (a macOS “package” file, actually a directory)
/// - `project.pbxproj` file
///
/// - Usage:
///
///     let accessor = Accessor() // use a concrete `Accessor` type
///     let runner = ScanRunner(accessor)
///     let report = try runner.scan()
///     // `report` contains an item for each rule violation found by the scan.
public struct ScanRunner {
    /// The file-system resource to be scanned.
    public var accessor: Accessor

    /// - Parameter url: A local file system `URL`. Should be one of:
    ///   - Xcode project directory (contains a `.xcodeproj` file)
    ///   - `.xcodeproj` directory (a macOS “package” file, actually a directory)
    ///   - `project.pbxproj` file.
    public init(_ url: URL) throws {
        try self.accessor = AccessorHelper.accessor(for: url)
    }

    /// - Parameter accessor: Should be one of:
    ///   - Xcode project directory (contains a `.xcodeproj` file)
    ///   - `.xcodeproj` directory (a macOS “package” file, actually a directory)
    ///   - `project.pbxproj` file.
    public init(_ accessor: Accessor) {
        self.accessor = accessor
    }

    /// Perform a scan of the resource, identifying any rule violations
    /// - Returns: A `Report`, with an item for each violation found by the scan.
    public func scan() throws -> Report {
        guard accessor.exists else {
            throw ScanRunnerError.accessorDoesNotExist
        }
        // we know accessor exists if we reach this point
        guard var target = self.accessor.target else {
            throw ScanRunnerError.unrecognizedAccessor
        }
        let report = try target.scan()
        target.accessor.report = report
        report.timestamp = Date()
        return report
    }
}
