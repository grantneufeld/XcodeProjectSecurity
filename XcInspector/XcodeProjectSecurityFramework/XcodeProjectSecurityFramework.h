// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#import <Foundation/Foundation.h>

//! Project version number for XcodeProjectSecurityFramework.
FOUNDATION_EXPORT double XcodeProjectSecurityFrameworkVersionNumber;

//! Project version string for XcodeProjectSecurityFramework.
FOUNDATION_EXPORT const unsigned char XcodeProjectSecurityFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <XcodeProjectSecurityFramework/PublicHeader.h>
