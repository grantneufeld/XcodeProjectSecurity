// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

// swiftlint:disable indentation_width

final class CommandLineInterfaceTests: XCTestCase {
    func test_help_returnsTheHelpInfo() async throws {
        let result = try await call(["-h"])
        let expected =
            """
            OVERVIEW: XcInspector scans Xcode project files for possible security issues.

            USAGE: xcinspector [--stats] [--details] <filepath>

            ARGUMENTS:
              <filepath>              Path to Xcode project directory, or .xcodeproj file.

            OPTIONS:
              -s, --stats             Show statistics at the end of the report.
              -d, --details           Show expanded details with warnings.
              --version               Show the version.
              -h, --help              Show help information.


            """
        XCTAssertEqual(result, expected)
    }

    // MARK: - version number

    func test_version_returnsTheVersionNumber() async throws {
        let result = try await call(["--version"])
        let expected = "0.1.0\n"
        XCTAssertEqual(result, expected)
    }

    // MARK: - filepath arguments

    func test_filepath_whenEmpty_outputsErrorToStdErr() async throws {
        let result = try await callWithError([])
        XCTAssertTrue(result.hasPrefix("Error: "))
    }

    func test_filepath_whenNonExistentFilepath_outputsErrorToStdErr() async throws {
        let result = try await callWithError(["does/not/exist"])
        XCTAssertTrue(result.hasPrefix("Error: "))
    }

    func test_filepath_whenFilePathIsNonXcodeProjectDirectory_outputsErrorToStdErr() async throws {
        let result = try await callWithError([SamplesHelpers.nonProjectURL.relativePath])
        XCTAssertTrue(result.hasPrefix("Error: "))
    }

    func test_filepath_whenFilePathIsXcodeProjectDirectory_hasNoError() async throws {
        let result = try await callWithError([SamplesHelpers.testProjectURL.relativePath])
        XCTAssertTrue(result.isEmpty)
    }

    func test_filepath_whenFilePathIsXcodeProjectDirectory_returnsReport() async throws {
        let result = try await call([SamplesHelpers.testProjectURL.relativePath])
        var expected = "\(SamplesHelpers.executableAccessor.fullPath):warning:\(ExecutableFilesRule().message)\n"
        expected.append("\(SamplesHelpers.pbxprojAccessor.fullPath):5:warning:\(EmbeddedScriptsRule().message)\n")
        expected.append("\n")
        XCTAssertEqual(result, expected)
    }

    // MARK: - Stats

    func test_stats_givenZeroViolations_showsZeroStats() async throws {
        let result = try await call(["--stats", SamplesHelpers.sandboxFlagYesPbxprojURL.relativePath])
        let expected = "Found 0 warnings.\n"
        XCTAssertEqual(result, expected)
    }

    func test_stats_withShortFlag_givenZeroViolations_showsZeroStats() async throws {
        let result = try await call(["-s", SamplesHelpers.sandboxFlagYesPbxprojURL.relativePath])
        let expected = "Found 0 warnings.\n"
        XCTAssertEqual(result, expected)
    }

    func test_stats_whenFilePathIsXcodeProjectDirectory_returnsReportWithStats() async throws {
        let result = try await call(["--stats", SamplesHelpers.testProjectURL.relativePath])
        var expected = "\(SamplesHelpers.executableAccessor.fullPath):warning:\(ExecutableFilesRule().message)\n"
        expected.append("\(SamplesHelpers.pbxprojAccessor.fullPath):5:warning:\(EmbeddedScriptsRule().message)\n")
        expected.append("\nFound 2 warnings.\n")
        XCTAssertEqual(result, expected)
    }

    // MARK: - Details

    func test_details_givenEmbeddedScript_showsTheScript() async throws {
        let result = try await call(["--details", SamplesHelpers.testProjectURL.relativePath])
        var expected = "\(SamplesHelpers.executableAccessor.fullPath):warning:\(ExecutableFilesRule().message)\n"
        expected.append("\(SamplesHelpers.pbxprojAccessor.fullPath):5:warning:\(EmbeddedScriptsRule().message)\n")
        expected.append("    # this is a shell script\n")
        expected.append("\n")
        XCTAssertEqual(result, expected)
    }

    // MARK: - Helpers

    /// The filename of the executable binary for the command line tool.
    private var filename = "xcinspector"

    /// The local filesystem URL for the executable command line tool binary.
    private var commandURL: URL {
        let currentFile = URL(filePath: #filePath)
        let commandlineTestsFolder = currentFile.deletingLastPathComponent() // CommandLineInterfaceTests.swift
        let testsFolder = commandlineTestsFolder.deletingLastPathComponent() // CommandLineInterfaceTests/
        let projectFolder = testsFolder.deletingLastPathComponent() // Tests/
        let productsFolder = projectFolder.appending(path: "Products")
        return productsFolder.appending(path: self.filename)
    }

    /// The filepath of the executable command line tool binary, in the local filesystem.
    private var commandPath: String {
        self.commandURL.relativePath
    }

    private enum CommandLineInterfaceTestsError: Error {
    case missingExecutableBinary
    }

    /// Thread-safe text accumulator.
    private actor Receiver {
        /// The accumulated text.
        var result = ""

        /// Add text to the `result`.
        /// - Parameter text: The text to append.
        func append(_ text: String) async {
            self.result.append(text)
        }
    }

    /// Call the executable command line tool binary.
    /// - Parameter arguments: An array of strings for the arguments to pass to the command line tool.
    /// - Returns: A `String` of anything the command outputs to `stdout`. (The text returned from the command.)
    /// - Throws: `CommandLineInterfaceTestsError.missingExecutableBinary` if the binary is missing.
    ///   Any `Process` errors when trying to run the command.
    private func call(_ arguments: [String]) async throws -> String {
        guard FileManager.default.fileExists(atPath: self.commandPath) else {
            throw CommandLineInterfaceTestsError.missingExecutableBinary
        }

        let receiver = Receiver()

        let captureOut = Pipe()
        captureOut.fileHandleForReading.readabilityHandler = { pipe in
            let line = String(decoding: pipe.availableData, as: UTF8.self)
            Task {
                await receiver.append(line)
            }
        }

        let process = Process()
        process.executableURL = self.commandURL
        process.arguments = arguments
        process.standardOutput = captureOut
        try process.run()
        process.waitUntilExit()

        return await receiver.result
    }

    /// Call the executable command line tool binary, capturing the error output.
    /// - Parameter arguments: An array of strings for the arguments to pass to the command line tool.
    /// - Returns: A `String` of anything the command outputs to `stderr`. (The error text returned from the command.)
    /// - Throws: `CommandLineInterfaceTestsError.missingExecutableBinary` if the binary is missing.
    ///   Any `Process` errors when trying to run the command.
    private func callWithError(_ arguments: [String]) async throws -> String {
        guard FileManager.default.fileExists(atPath: self.commandPath) else {
            throw CommandLineInterfaceTestsError.missingExecutableBinary
        }

        let receiver = Receiver()

        let captureError = Pipe()
        captureError.fileHandleForReading.readabilityHandler = { pipe in
            let line = String(decoding: pipe.availableData, as: UTF8.self)
            Task {
                await receiver.append(line)
            }
        }

        let process = Process()
        process.executableURL = self.commandURL
        process.arguments = arguments
        process.standardError = captureError
        try process.run()
        process.waitUntilExit()

        return await receiver.result
    }
}

// swiftlint:enable indentation_width
