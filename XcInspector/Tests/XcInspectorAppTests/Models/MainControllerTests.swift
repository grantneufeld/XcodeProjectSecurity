// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

@testable import XcInspectorApp
import XcodeProjectSecurityFramework
import XCTest

final class MainControllerTests: XCTestCase {
    // MARK: .error

    func test_error_defaultsToNil() throws {
        let sut = MainController()
        XCTAssertNil(sut.error)
    }

    func test_error_canBeSet() throws {
        let sut = MainController()
        sut.error = ScanRunnerError.accessorDoesNotExist
        XCTAssertEqual(sut.error as? ScanRunnerError, .accessorDoesNotExist)
    }

    // MARK: .hasError

    func test_hasError_defaultsToFalse() throws {
        let sut = MainController()
        XCTAssertFalse(sut.hasError)
    }

    func test_hasError_canBeSet() throws {
        let sut = MainController()
        sut.hasError = true
        XCTAssertTrue(sut.hasError)
    }

    // MARK: .openAction

    func test_openAction_defaultsToFalse() throws {
        let sut = MainController()
        XCTAssertFalse(sut.openAction)
    }

    func test_openAction_canBeSetTrue() throws {
        let sut = MainController()
        sut.openAction = true
        XCTAssertTrue(sut.openAction)
    }

    // MARK: .saveAction

    func test_saveAction_defaultsToFalse() throws {
        let sut = MainController()
        XCTAssertFalse(sut.saveAction)
    }

    func test_saveAction_canBeSetTrue() throws {
        let sut = MainController()
        sut.saveAction = true
        XCTAssertTrue(sut.saveAction)
    }

    // MARK: .project

    func test_project_defaultsToNil() throws {
        let sut = MainController()
        XCTAssertNil(sut.project)
    }

    // MARK: .projects

    func test_projects_defaultsToEmptyCollection() throws {
        let sut = MainController()
        XCTAssertTrue(sut.projects.isEmpty)
    }

    // MARK: - Methods

    func test_reportError_setsTheError() throws {
        let sut = MainController()
        sut.reportError(error: ScanRunnerError.accessorDoesNotExist)
        XCTAssertEqual(sut.error as? ScanRunnerError, .accessorDoesNotExist)
    }

    func test_reportError_setsHasErrorToTrue() throws {
        let sut = MainController()
        sut.reportError(error: ScanRunnerError.accessorDoesNotExist)
        XCTAssertTrue(sut.hasError)
    }

    func test_hideError_leavesHasErrorFalse() throws {
        let sut = MainController()
        sut.hideError()
        XCTAssertFalse(sut.hasError)
    }

    func test_hideError_givenHasErrorIsTrue_setsHasErrorFalse() throws {
        let sut = MainController()
        sut.hasError = true
        sut.hideError()
        XCTAssertFalse(sut.hasError)
    }

    func test_open_givenNonProjectURL_reportsError() throws {
        let sut = MainController()
        sut.open(SamplesHelpers.nonProjectURL)
        XCTAssertEqual(sut.error as? ScanRunnerError, .unrecognizedAccessor)
    }

    func test_open_givenFileWithRuleViolations_reportsViolations() throws {
        let sut = MainController()
        sut.open(SamplesHelpers.pbxprojURL)
        XCTAssertEqual(sut.project?.report?.count, 1)
    }

    func test_open_appendsAccessorToProjects() throws {
        let sut = MainController()
        sut.open(SamplesHelpers.pbxprojURL)
        XCTAssertEqual(sut.projects.count, 1)
    }

    // MARK: - save

    func test_save_whenNoProject_throwsError() throws {
        let sut = MainController()
        let url = URL(filePath: "").appendingPathComponent("should_not_save.txt")
        XCTAssertThrowsError(try sut.save(url)) { error in
            XCTAssertEqual(error as? MainError, .noProject)
        }
    }

    func test_save_whenProjectWithoutReport_throwsError() throws {
        let project = Project(accessor: SamplesHelpers.executableAccessor)
        let sut = MainController()
        sut.project = project
        let url = URL(filePath: "").appendingPathComponent("should_not_save.txt")
        XCTAssertThrowsError(try sut.save(url)) { error in
            XCTAssertEqual(error as? MainError, .noReport)
        }
    }

    func test_save_whenProjectWithReport_savesTheReport() throws {
        // Setup
        let accessor = SamplesHelpers.pbxprojAccessor
        let runner = ScanRunner(accessor)
        let report = try runner.scan()
        let project = Project(accessor: accessor, report: report)

        let sut = MainController()
        sut.project = project

        let cacheURLs = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        let cacheURL = try XCTUnwrap(cacheURLs.first)
        let url = cacheURL.appendingPathComponent("MainController_save_test.txt")
        defer {
            // cleanup
            if FileManager.default.fileExists(atPath: url.relativePath) {
                try? FileManager.default.removeItem(at: url)
            }
        }

        // Action
        try sut.save(url)

        // Result
        XCTAssertTrue(FileManager.default.fileExists(atPath: url.relativePath))
        let result = try String(contentsOf: url)
        XCTAssertTrue(
            result.hasSuffix("project.pbxproj:5:warning:Found an embedded script in the pbxproj file.\n")
        )
    }
}
