// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class StringExtensionsTests: XCTestCase {
    // MARK: .decodeSlashEncodedCharacters

    func test_decodeSlashEncodedCharacters_givenEmptyString_returnsEmptyString() {
        let sut = ""
        let result = sut.decodeSlashEncodedCharacters
        XCTAssertEqual(result, "")
    }

    func test_decodeSlashEncodedCharacters_givenNoSlashEncoding_returnsSameString() {
        let sut = "No slash encoding."
        let result = sut.decodeSlashEncodedCharacters
        XCTAssertEqual(result, "No slash encoding.")
    }

    func test_decodeSlashEncodedCharacters_givenSlashWithN_returnsLinefeedCharacter() {
        let sut = "\\n"
        let result = sut.decodeSlashEncodedCharacters
        XCTAssertEqual(result, "\n")
    }

    func test_decodeSlashEncodedCharacters_givenSlashWithR_returnsCRCharacter() {
        let sut = "\\r"
        let result = sut.decodeSlashEncodedCharacters
        XCTAssertEqual(result, "\r")
    }

    func test_decodeSlashEncodedCharacters_givenSlashWithQuote_returnsQuoteCharacter() {
        let sut = "\\\""
        let result = sut.decodeSlashEncodedCharacters
        XCTAssertEqual(result, "\"")
    }

    func test_decodeSlashEncodedCharacters_givenStringIsTwoSlashes_returnsSingleSlash() {
        let sut = "\\\\"
        let result = sut.decodeSlashEncodedCharacters
        XCTAssertEqual(result, "\\")
    }

    func test_decodeSlashEncodedCharacters_givenALotOfEncodedCharacters_returnsExpectedString() {
        let sut = "\\\\\\n\\r\\\"\\\\"
        let result = sut.decodeSlashEncodedCharacters
        XCTAssertEqual(result, "\\\n\r\"\\")
    }
}
