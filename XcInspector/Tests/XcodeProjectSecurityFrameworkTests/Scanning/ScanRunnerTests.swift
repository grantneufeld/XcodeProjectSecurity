// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class ScanRunnerTests: XCTestCase {
    // MARK: - Initialization

    func test_init_givenNonProjectDirectoryURL_() throws {
        let url = SamplesHelpers.nonProjectURL
        let sut = try ScanRunner(url)
        let result = try XCTUnwrap(sut.accessor as? DirectoryAccessor)
        XCTAssertEqual(result, SamplesHelpers.nonProjectAccessor)
    }

    func test_accessor_canBeSetAtInit() throws {
        let directory = SamplesHelpers.testProjectAccessor
        let sut = ScanRunner(directory)
        XCTAssertEqual(sut.accessor as? DirectoryAccessor, directory)
    }

    // MARK: - scan()

    func test_scan_givenNonExistentDirectory_throwsError() throws {
        let url = URL(fileURLWithPath: "non-existent")
        let directory = DirectoryAccessor(url: url)
        let sut = ScanRunner(directory)
        XCTAssertThrowsError(try sut.scan()) { error in
            XCTAssertEqual(error as? ScanRunnerError, .accessorDoesNotExist)
        }
    }

    func test_scan_givenValidDirectory_doesNotThrowError() throws {
        let directory = SamplesHelpers.testProjectAccessor
        let sut = ScanRunner(directory)
        XCTAssertNoThrow(try sut.scan())
    }

    func test_scan_givenNonProjectDirectory_throwsError() throws {
        let directory = SamplesHelpers.nonProjectAccessor
        let sut = ScanRunner(directory)
        XCTAssertThrowsError(try sut.scan()) { error in
            XCTAssertEqual(error as? ScanRunnerError, .unrecognizedAccessor)
        }
    }

    func test_scan_givenPBXProjWithEmbeddedScript_returnsViolationInArray() throws {
        let accessor = SamplesHelpers.pbxprojAccessor
        let sut = ScanRunner(accessor)
        let report = try sut.scan()
        let expectedViolation = RuleViolation(
            rule: EmbeddedScriptsRule(),
            file: accessor.fullPath,
            line: 5,
            extraMessage: nil,
            data: "# this is a shell script"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
    }

    func test_scan_assignsReportToAccessor() throws {
        let directory = SamplesHelpers.testProjectAccessor
        let sut = ScanRunner(directory)
        _ = try sut.scan()
        XCTAssertEqual(directory.report?.count, SamplesHelpers.testProjectViolationCount)
    }

    func test_scan_assignsTimestampForReport() throws {
        let directory = SamplesHelpers.testProjectAccessor
        let sut = ScanRunner(directory)
        let beforeTime = Date()
        let report = try sut.scan()
        let afterTime = Date()
        XCTAssertTrue(beforeTime < report.timestamp)
        XCTAssertTrue(report.timestamp <= afterTime)
    }
}
