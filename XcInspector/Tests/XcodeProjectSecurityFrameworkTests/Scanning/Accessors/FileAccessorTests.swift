// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class FileAccessorTests: XCTestCase {
    // MARK: .report

    func test_report_defaultsToNil() throws {
        let url = SamplesHelpers.testProjectURL
        let sut = FileAccessor(url: url)
        XCTAssertNil(sut.report)
    }

    func test_report_canBeAssigned() throws {
        let url = SamplesHelpers.testProjectURL
        let sut = FileAccessor(url: url)
        let report = Report()
        let violation = RuleViolation(rule: ExecutableFilesRule(), file: "/assigned")
        report.append(violation)
        sut.report = report
        XCTAssertEqual(sut.report?.first?.file, "/assigned")
    }

    // MARK: - isExecutable

    func test_isExecutable_givenExecutableFile_returnsTrue() throws {
        let sut = SamplesHelpers.executableAccessor
        let result = sut.isExecutable
        XCTAssertTrue(result)
    }

    func test_isExecutable_givenNonExecutableFile_returnsFalse() throws {
        let sut = SamplesHelpers.nonProjectReadmeAccessor
        let result = sut.isExecutable
        XCTAssertFalse(result)
    }

    // MARK: - .hasExecutableBit

    func test_hasExecutableBit_givenExecutableFile_returnsTrue() throws {
        let sut = SamplesHelpers.executableAccessor
        let result = sut.hasExecutableBit
        XCTAssertTrue(result)
    }

    func test_hasExecutableBit_givenNonExecutableFile_returnsFalse() throws {
        let sut = SamplesHelpers.nonProjectReadmeAccessor
        let result = sut.hasExecutableBit
        XCTAssertFalse(result)
    }

    // MARK: - Initialization

    func test_init_acceptsURL() throws {
        let sut = FileAccessor(url: SamplesHelpers.nonProjectReadmeURL)
        XCTAssertEqual(sut.url, SamplesHelpers.nonProjectReadmeURL)
    }

    // MARK: - Computed Properties

    // MARK: .filename

    func test_filename_isTheFilenamePortionOfTheGivenPath() {
        let sut = FileAccessor(url: self.url(for: "/some/sub/directory/expected.filename"))
        XCTAssertEqual(sut.filename, "expected.filename")
    }

    // MARK: - Equatable

    func test_equality_givenURLsWithEmptyPaths_returnsTrue() {
        let url = URL(filePath: "")
        let sut = FileAccessor(url: url)
        let other = FileAccessor(url: url)
        XCTAssertEqual(sut, other)
    }

    func test_equality_givenMatchingPaths_returnsTrue() {
        let url = URL(filePath: "matching/path")
        let sut = FileAccessor(url: url)
        let other = FileAccessor(url: url)
        XCTAssertEqual(sut, other)
    }

    func test_equality_givenDifferentPaths_returnsFalse() {
        let url1 = URL(filePath: "first.txt")
        let url2 = URL(filePath: "different.file")
        let sut = FileAccessor(url: url1)
        let other = FileAccessor(url: url2)
        XCTAssertNotEqual(sut, other)
    }

    // MARK: - File Data Access

    // MARK: read()

    func test_read_withMissingFile_throwsError() throws {
        // Given
        let url = self.url(for: "missing.file")
        do { try FileManager.default.removeItem(at: url) } catch {}
        // When
        let sut = FileAccessor(url: url)
        XCTAssertThrowsError(try sut.read())
        // cleanup
        do { try FileManager.default.removeItem(at: url) } catch {}
    }

    func test_read_withExistingFile_returnsTheFileContentAsString() throws {
        // Given
        let expectedText = "Read this!"
        let url = url(for: "read.this")
        do { try FileManager.default.removeItem(at: url) } catch {}
        let textData = expectedText.data(using: .utf8)!
        try textData.write(to: url, options: .atomic)
        // When
        let sut = FileAccessor(url: url)
        let readText = try sut.read()
        // Then
        XCTAssertEqual(readText, expectedText)
        // cleanup
        do { try FileManager.default.removeItem(at: url) } catch {}
    }

    // MARK: overwrite()

    func test_overwrite_whenFileDoesNotExist_createsAndSetsTheFileContent() throws {
        // Given
        let outputURL = self.url(for: "write_output.txt")
        // When
        let sut = FileAccessor(url: outputURL)
        let outputText = "This is the output."
        try sut.overwrite(outputText)
        // Then
        let writtenText = try String(contentsOf: outputURL, encoding: .utf8)
        XCTAssertEqual(writtenText, outputText)
        // cleanup
        try FileManager.default.removeItem(at: outputURL)
    }

    // MARK: .exists

    func test_exists_givenNonExistentPath_returnsFalse() {
        let sut = FileAccessor(url: URL(filePath: "this does not exist"))
        XCTAssertFalse(sut.exists)
    }

    func test_exists_givenExistingPath_returnsTrue() throws {
        let sut = FileAccessor(url: self.url(for: "file_exists.txt"))
        try sut.overwrite("This exists!")
        XCTAssertTrue(sut.exists)
        // cleanup
        try sut.delete()
    }

    func test_exists_givenPathWithSpaceInName_returnsTrue() throws {
        let sut = FileAccessor(url: self.url(for: "space in name.txt"))
        try sut.overwrite("This exists with space in name!")
        XCTAssertTrue(sut.exists)
        // cleanup
        try sut.delete()
    }

    // MARK: delete()

    func test_delete_givenAFile_removesTheFile() throws {
        // Given
        let outputURL = self.url(for: "delete_this.txt")
        let sut = FileAccessor(url: outputURL)
        try sut.overwrite("Delete this!")
        // When
        try sut.delete()
        XCTAssertFalse(sut.exists)
    }

    func test_delete_givenNonExistentFilePath_throwsError() throws {
        let sut = FileAccessor(url: URL(filePath: "does not exist"))
        XCTAssertThrowsError(try sut.delete())
    }

    // MARK: - Target

    func test_target_givenNonProjectFile_returnsNil() throws {
        let sut = SamplesHelpers.nonProjectReadmeAccessor
        XCTAssertNil(sut.target)
    }

    func test_target_givenPbxprojFile_returnsPBXProjTarget() throws {
        let sut = SamplesHelpers.pbxprojAccessor
        let target = sut.target
        XCTAssertNotNil(target as? PBXProjTarget)
    }

    // MARK: - Test Helpers

    /// URL for the user’s Cache directory.
    private let cacheDirURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).last!
    /// URL for the root (top-most) directory.
    private let rootDirURL = URL(fileURLWithPath: "/")

    /// Given a path, generate an `URL` for it within the cache directory.
    /// - Parameter path: A filepath, under the cache directory.
    /// - Returns: The `URL` for the path within the cache directory.
    private func urlFrom(path: String) -> URL? {
        URL(fileURLWithPath: path)
    }

    private func url(for filename: String) -> URL {
        self.cacheDirURL.appendingPathComponent(filename)
    }
}
