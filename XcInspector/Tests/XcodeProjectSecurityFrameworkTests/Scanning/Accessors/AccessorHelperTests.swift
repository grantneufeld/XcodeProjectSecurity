// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class AccessorHelperTests: XCTestCase {
    func test_accessor_givenHTTPURL_throwsError() throws {
        let url = URL(string: "http://localhost/")!
        XCTAssertThrowsError(try AccessorHelper.accessor(for: url)) { error in
            XCTAssertEqual(error as? AccessorError, .invalidURL)
        }
    }

    func test_accessor_givenDirectoryURL_returnsDirectoryAccessor() throws {
        let url = SamplesHelpers.testProjectURL
        let sut = try AccessorHelper.accessor(for: url)
        XCTAssertNotNil(sut as? DirectoryAccessor)
    }

    func test_accessor_givenDirectoryURL_givesAccessorExpectedPath() throws {
        let url = SamplesHelpers.testProjectURL
        let sut = try AccessorHelper.accessor(for: url)
        let expectedPath = url.relativePath
        XCTAssertEqual(sut.fullPath, expectedPath)
    }

    func test_accessor_givenFileURL_returnsFileAccessor() throws {
        let url = SamplesHelpers.nonProjectReadmeURL
        let sut = try AccessorHelper.accessor(for: url)
        XCTAssertNotNil(sut as? FileAccessor)
    }

    func test_accessor_givenFileURL_givesAccessorExpectedPath() throws {
        let url = SamplesHelpers.nonProjectReadmeURL
        let sut = try AccessorHelper.accessor(for: url)
        let expectedPath = url.relativePath
        XCTAssertEqual(sut.fullPath, expectedPath)
    }
}
