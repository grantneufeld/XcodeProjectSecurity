// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class DirectoryAccessorTests: XCTestCase {
    // MARK: .report

    func test_report_defaultsToNil() throws {
        let url = SamplesHelpers.testProjectURL
        let sut = DirectoryAccessor(url: url)
        XCTAssertNil(sut.report)
    }

    func test_report_canBeAssigned() throws {
        let url = SamplesHelpers.testProjectURL
        let sut = DirectoryAccessor(url: url)
        let report = Report()
        let violation = RuleViolation(rule: ExecutableFilesRule(), file: "/assigned")
        report.append(violation)
        sut.report = report
        XCTAssertEqual(sut.report?.first?.file, "/assigned")
    }

    // MARK: - Initialization

    func test_init_acceptsURL() throws {
        let url = SamplesHelpers.testProjectURL
        let sut = DirectoryAccessor(url: url)
        XCTAssertEqual(sut.url, url)
    }

    // MARK: - Computed Properties

    // MARK: .url

    func test_url_returnsURLBasedOnPath() {
        let sut = DirectoryAccessor(url: URL(fileURLWithPath: "/directory/sut/"))
        XCTAssertEqual(sut.url.absoluteString, "file:///directory/sut/")
    }

    // MARK: .filename

    func test_filename_isTheFilenamePortionOfTheGivenPath() {
        let sut = DirectoryAccessor(url: self.url(for: "/some/sub/directory/expected/"))
        XCTAssertEqual(sut.filename, "expected")
    }

    // MARK: - Equatable

    func test_equality_givenEmptyPaths_returnsTrue() {
        let sut = DirectoryAccessor(url: self.url(for: ""))
        let other = DirectoryAccessor(url: self.url(for: ""))
        XCTAssertEqual(sut, other)
    }

    func test_equality_givenMatchingPaths_returnsTrue() {
        let sut = DirectoryAccessor(url: self.url(for: "matching/path/"))
        let other = DirectoryAccessor(url: self.url(for: "matching/path/"))
        XCTAssertEqual(sut, other)
    }

    func test_equality_givenDifferentPaths_returnsFalse() {
        let sut = DirectoryAccessor(url: self.url(for: "first/"))
        let other = DirectoryAccessor(url: self.url(for: "different/"))
        XCTAssertNotEqual(sut, other)
    }

    // MARK: - File Data Access

    // MARK: .exists

    func test_exists_givenNonExistentPath_returnsFalse() {
        let sut = DirectoryAccessor(url: self.url(for: "this does not exist"))
        XCTAssertFalse(sut.exists)
    }

    func test_exists_givenExistingPath_returnsTrue() throws {
        let sut = DirectoryAccessor(url: self.url(for: "dir_exists"))
        try sut.ensureExists()
        XCTAssertTrue(sut.exists)
        // cleanup
        try sut.delete()
    }

    func test_exists_givenSpacesInPath_returnsTrue() throws {
        let sut = DirectoryAccessor(url: self.url(for: "space in name"))
        try sut.ensureExists()
        XCTAssertTrue(sut.exists)
        // cleanup
        try sut.delete()
    }

    // MARK: .count

    func test_count_givenEmptyFolder_returnsZero() throws {
        let folderURL = self.url(for: "EmptyFolder")
        let sut = DirectoryAccessor(url: folderURL)
        try sut.ensureExists()
        XCTAssertEqual(sut.count, 0)
        // cleanup
        try sut.delete()
    }

    func test_count_givenFolderWithOneDocument_returnsOne() throws {
        let sut = SamplesHelpers.nonProjectAccessor
        XCTAssertEqual(sut.count, 1)
    }

    // MARK: ensureExists()

    func test_ensureExists_givenDirectoryDoesNotExist_createsTheDirectory() throws {
        let sut = DirectoryAccessor(url: self.url(for: "/ensure_exists"))
        try sut.ensureExists()
        XCTAssertTrue(sut.exists)
        // cleanup
        try sut.delete()
    }

    // MARK: delete()

    func test_delete_givenAFile_removesTheDirectory() throws {
        // Given
        let outputURL = self.url(for: "delete_this/")
        let sut = DirectoryAccessor(url: outputURL)
        try sut.ensureExists()
        // When
        try sut.delete()
        XCTAssertFalse(sut.exists)
    }

    func test_delete_givenNonExistentDirectoryPath_throwsError() throws {
        let sut = DirectoryAccessor(url: self.url(for: "does not exist"))
        XCTAssertThrowsError(try sut.delete())
    }

    // MARK: - Sequence

    func test_sequence_givenMultipleItemsOfDifferentTypes_returnsExpectedAccessors() throws {
        let url = self.url(for: "/iterate")
        let sut = DirectoryAccessor(url: url)
        let childURL = url.appendingPathComponent("/child/")
        let child = DirectoryAccessor(url: childURL)
        try child.ensureExists()
        let docURL = url.appendingPathComponent("/doc.txt")
        let doc = FileAccessor(url: docURL)
        try doc.overwrite("")
        var result: [Accessor] = []
        for resource in sut {
            result.append(resource)
        }
        XCTAssertNotNil(result[1] as? DirectoryAccessor)
        XCTAssertNotNil(result[0] as? FileAccessor)
        XCTAssertTrue(result[1].fullPath.hasSuffix("/iterate/child"))
        XCTAssertTrue(result[0].fullPath.hasSuffix("/iterate/doc.txt"))
    }

    func test_sequence_givenMultipleSublevelsOfDirectories_onlyIteratesOverTopLevel() throws {
        let sut = SamplesHelpers.testProjectAccessor
        var filenames: [String] = []
        for resource in sut {
            if resource.filename != ".DS_Store" {
                filenames.append(resource.filename)
            }
        }
        XCTAssertEqual(filenames.sorted(), ["README.md", "TestProject.xcodeproj", "executable.sh"])
    }

    // MARK: - Target

    func test_target_givenNonTargetDirectory_returnsNil() throws {
        let sut = SamplesHelpers.nonProjectAccessor
        XCTAssertNil(sut.target)
    }

    func test_target_givenProjectDirectory_returnsProjectDirectoryTarget() throws {
        let sut = SamplesHelpers.testProjectAccessor
        let target = sut.target
        XCTAssertNotNil(target as? ProjectDirectoryTarget)
    }

    func test_target_givenXcodeProj_returnsXcodeProjTarget() throws {
        let sut = SamplesHelpers.xcodeprojAccessor
        let target = sut.target
        XCTAssertNotNil(target as? XcodeProjTarget)
    }

    // MARK: - Test Helpers

    /// URL for the user’s Cache directory.
    private let cacheDirURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).last!

    private func url(for filename: String) -> URL {
        self.cacheDirURL.appendingPathComponent(filename)
    }
}
