// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class ProjectTests: XCTestCase {
    func test_init_givenAccessor_assignsAccessor() throws {
        let sut = Project(accessor: SamplesHelpers.testProjectAccessor)
        XCTAssertEqual(sut.accessor as? DirectoryAccessor, SamplesHelpers.testProjectAccessor)
    }

    func test_init_givenNoReport_reportDefaultsToNil() throws {
        let sut = Project(accessor: SamplesHelpers.testProjectAccessor)
        XCTAssertNil(sut.report)
    }

    func test_init_givenReport_assignsReport() throws {
        let report = Report()
        report.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "/test/file"))
        let sut = Project(accessor: SamplesHelpers.testProjectAccessor, report: report)
        XCTAssertEqual(sut.report, report)
    }

    // MARK: - Identifiable

    func test_id_returnsURLFromAccessor() throws {
        let sut = Project(accessor: SamplesHelpers.executableAccessor)
        XCTAssertEqual(sut.id, SamplesHelpers.executableURL)
    }

    // MARK: - Equatable

    func test_equality_givenSameAccessorsAndNoReports_returnsTrue() throws {
        let sut = Project(accessor: SamplesHelpers.pbxprojAccessor)
        let other = Project(accessor: SamplesHelpers.pbxprojAccessor)
        XCTAssertEqual(sut, other)
    }

    func test_equality_givenSameAccessorsDifferentReports_returnsFalse() throws {
        let report1 = Report()
        report1.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "/first"))
        let sut = Project(accessor: SamplesHelpers.pbxprojAccessor, report: report1)
        let report2 = Report()
        report2.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "/second"))
        let other = Project(accessor: SamplesHelpers.pbxprojAccessor, report: report2)
        XCTAssertNotEqual(sut, other)
    }

    func test_equality_givenDifferentAccessors_returnsFalse() throws {
        let sut = Project(accessor: SamplesHelpers.xcodeprojAccessor)
        let other = Project(accessor: SamplesHelpers.nonProjectAccessor)
        XCTAssertNotEqual(sut, other)
    }

    // MARK: - Hashable

    func test_hash_returnsExpectedValue() throws {
        let sut = Project(accessor: SamplesHelpers.xcodeprojAccessor)
        XCTAssertEqual(sut.hashValue, SamplesHelpers.xcodeprojAccessor.fullPath.hashValue)
    }
}
