// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

struct MockRule: Rule {
    var uuid = UUID()
    var message: String { "Mock Rule" }

    func scan(target: XcodeProjectSecurityFramework.Accessor) throws -> Report {
        Report()
    }

    func equals(_ other: Rule) -> Bool {
        self.uuid == (other as? Self)?.uuid
    }
}

final class RuleViolationTests: XCTestCase {
    func test_id_isSetOnCreate() throws {
        let sut = RuleViolation(rule: MockRule(), file: "id")
        XCTAssertNotEqual(sut.id, UUID())
    }

    func test_rule_givenAssignedAValue_returnsTheValue() {
        let rule = MockRule()
        let sut = RuleViolation(rule: rule, file: "")
        XCTAssertTrue(sut.rule.equals(rule))
    }

    func test_file_givenAssignedFilnemae_returnsFilename() {
        let sut = RuleViolation(rule: MockRule(), file: "file.txt")
        XCTAssertEqual(sut.file, "file.txt")
    }

    func test_line_canBeAssignedAsNil() {
        let sut = RuleViolation(rule: MockRule(), file: "", line: nil)
        XCTAssertNil(sut.line)
    }

    func test_line_givenAssigned42_returns42() {
        let sut = RuleViolation(rule: MockRule(), file: "", line: 42)
        XCTAssertEqual(sut.line, 42)
    }

    func test_extraMessage_givenAssignedNil_returnsNil() {
        let sut = RuleViolation(rule: MockRule(), file: "", extraMessage: nil)
        XCTAssertNil(sut.extraMessage)
    }

    func test_extraMessage_givenAssignedEmptyString_returnsEmptyString() {
        let sut = RuleViolation(rule: MockRule(), file: "", extraMessage: "")
        XCTAssertEqual(sut.extraMessage, "")
    }

    func test_extraMessage_givenAssignedSomeDetails_returnsThoseDetails() {
        let sut = RuleViolation(rule: MockRule(), file: "", extraMessage: "Some details.")
        XCTAssertEqual(sut.extraMessage, "Some details.")
    }

    func test_data_givenAssignedNil_returnsNil() {
        let sut = RuleViolation(rule: MockRule(), file: "", data: nil)
        XCTAssertNil(sut.data)
    }

    func test_data_givenAssignedSomeData_returnsTheData() {
        let sut = RuleViolation(rule: MockRule(), file: "", data: "Some data.")
        XCTAssertEqual(sut.data, "Some data.")
    }

    // MARK: - Equatable

    func test_equality_givenBothHaveEmptyOrNilValues_returnsTrue() {
        let rule = MockRule()
        let sut = RuleViolation(rule: rule, file: "")
        let other = RuleViolation(rule: rule, file: "")
        XCTAssertEqual(sut, other)
    }

    func test_equality_givenDifferentRules_returnsFalse() {
        let firstRule = MockRule()
        let sut = RuleViolation(rule: firstRule, file: "")
        let secondRule = MockRule()
        let other = RuleViolation(rule: secondRule, file: "")
        XCTAssertNotEqual(sut, other)
    }

    // MARK: - plainText()

    func test_plainText_givenEmbeddedScriptsViolation_returnsTheText() throws {
        let sut = RuleViolation(rule: EmbeddedScriptsRule(), file: "violation", line: 23, data: "#!/bin/sh")
        let result = sut.plainText()
        XCTAssertEqual(result, "violation:23:warning:Found an embedded script in the pbxproj file.")
    }

    func test_plainText_givenExecutableFilesViolation_returnsTheText() throws {
        let sut = RuleViolation(rule: ExecutableFilesRule(), file: "script.sh")
        let result = sut.plainText()
        XCTAssertEqual(result, "script.sh:warning:Found an executable file in the project directory.")
    }

    func test_plainText_givenUserScriptSandboxingViolation_returnsTheText() throws {
        let sut = RuleViolation(rule: UserScriptSandboxingRule(), file: "project.pbxproj", line: 123)
        let result = sut.plainText()
        XCTAssertEqual(result, "project.pbxproj:123:warning:User Script Sandboxing flag not set in project.")
    }
}
