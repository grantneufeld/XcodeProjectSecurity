// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class ExecutableFilesRuleTests: XCTestCase {
    func test_message_isAsExpected() throws {
        let sut = ExecutableFilesRule()
        XCTAssertEqual(sut.message, "Found an executable file in the project directory.")
    }

    func test_scan_givenFileAccessor_throwsError() throws {
        let sut = ExecutableFilesRule()
        XCTAssertThrowsError(try sut.scan(target: SamplesHelpers.nonProjectReadmeAccessor)) { error in
            XCTAssertEqual(error as? RuleError, .targetDirectoryIsNotDirectory)
        }
    }

    func test_scan_directoryWithAnExectableFile_returnsRuleViolation() throws {
        let sut = ExecutableFilesRule()
        let report = try sut.scan(target: SamplesHelpers.testProjectAccessor)
        let violation = report.first
        let expected = RuleViolation(
            rule: ExecutableFilesRule(),
            file: SamplesHelpers.executableURL.relativePath,
            line: nil,
            extraMessage: nil,
            data: nil
        )
        XCTAssertEqual(violation, expected)
    }
}
