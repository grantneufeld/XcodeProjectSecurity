// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

struct RuleTestExample: Rule {
    var message: String { "" }

    func scan(target: Accessor) throws -> Report { Report() }

    func equals(_ other: XcodeProjectSecurityFramework.Rule) -> Bool {
        true
    }
}

final class RuleTests: XCTestCase {
    func test_message_defaultsToEmpty() {
        let sut = RuleTestExample()
        XCTAssertEqual(sut.message, "")
    }

    func test_scan_defaultsToEmptyArray() throws {
        let sut = RuleTestExample()
        let file = FileAccessor(url: SamplesHelpers.nonProjectReadmeURL)
        let report = try sut.scan(target: file)
        XCTAssertEqual(report, Report())
    }
}
