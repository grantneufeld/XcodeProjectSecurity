// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class UserScriptSandboxingRuleTests: XCTestCase {
    func test_message_isExpectedString() throws {
        let sut = UserScriptSandboxingRule()
        XCTAssertEqual(sut.message, "User Script Sandboxing flag not set in project.")
    }

    func test_scan_givenDirectoryAccessor_throwsError() throws {
        let sut = UserScriptSandboxingRule()
        XCTAssertThrowsError(try sut.scan(target: SamplesHelpers.testProjectAccessor)) { error in
            XCTAssertEqual(error as? RuleError, .targetFileIsNotFile)
        }
    }

    func test_scan_givenNonExistentFile_throwsError() throws {
        let sut = UserScriptSandboxingRule()
        XCTAssertThrowsError(try sut.scan(target: FileAccessor(url: URL(filePath: "non-existent")))) { error in
            XCTAssertEqual(error as? RuleError, .targetFileDoesNotExist)
        }
    }

    func test_scan_givenNonPbxprojFile_throwsError() throws {
        let sut = UserScriptSandboxingRule()
        XCTAssertThrowsError(try sut.scan(target: SamplesHelpers.nonProjectReadmeAccessor)) { error in
            XCTAssertEqual(error as? RuleError, .targetFileIsNotPbxproj)
        }
    }

    func test_scan_givenPbxprojWithMissingFlag_returnsViolation() throws {
        let sut = UserScriptSandboxingRule()
        let report = try sut.scan(target: SamplesHelpers.sandboxMissingPbxproj)
        let expectedViolation = RuleViolation(
            rule: UserScriptSandboxingRule(), file: SamplesHelpers.sandboxMissingPbxprojURL.relativePath
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
    }

    func test_scan_givenPbxprojWithYesFlag_returnsNoViolations() throws {
        let sut = UserScriptSandboxingRule()
        let report = try sut.scan(target: SamplesHelpers.sandboxFlagYesPbxproj)
        XCTAssertTrue(report.isEmpty)
    }

    func test_scan_givenPbxprojWithNoFlag_returnsViolation() throws {
        let sut = UserScriptSandboxingRule()
        let report = try sut.scan(target: SamplesHelpers.sandboxFlagNoPbxproj)
        let expectedViolation = RuleViolation(
            rule: UserScriptSandboxingRule(),
            file: SamplesHelpers.sandboxFlagNoPbxprojURL.relativePath,
            line: 2
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
    }

    func test_scan_givenPbxprojWithMultipleFlagsWhere2AreNo_returns2Violations() throws {
        let sut = UserScriptSandboxingRule()
        let report = try sut.scan(target: SamplesHelpers.sandboxFlagMultiplePbxprojAccessor)
        let expectedViolation1 = RuleViolation(
            rule: UserScriptSandboxingRule(),
            file: SamplesHelpers.sandboxFlagMultiplePbxprojURL.relativePath,
            line: 3
        )
        let expectedViolation2 = RuleViolation(
            rule: UserScriptSandboxingRule(),
            file: SamplesHelpers.sandboxFlagMultiplePbxprojURL.relativePath,
            line: 5
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation1)
        expectedReport.append(expectedViolation2)
        XCTAssertEqual(report, expectedReport)
    }

    // MARK: - equals()

    func test_equals_givenSameTypeOfRule_returnsTruet() throws {
        let sut = UserScriptSandboxingRule()
        XCTAssertTrue(sut.equals(UserScriptSandboxingRule()))
    }

    func test_equals_givenOtherTypeOfRule_returnsFalse() throws {
        let sut = UserScriptSandboxingRule()
        let other = MockRule()
        XCTAssertFalse(sut.equals(other))
    }
}
