// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

// swiftlint:disable:next type_body_length
final class EmbeddedScriptsRuleTests: XCTestCase {
    func test_message_returnsTheExpectedText() {
        let sut = EmbeddedScriptsRule()
        XCTAssertEqual(sut.message, "Found an embedded script in the pbxproj file.")
    }

    // MARK: - scan()

    func test_scan_givenDirectoryAccessor_throwsError() throws {
        let sut = EmbeddedScriptsRule()
        XCTAssertThrowsError(try sut.scan(target: SamplesHelpers.nonProjectAccessor)) { error in
            XCTAssertEqual(error as? RuleError, RuleError.targetFileIsNotFile)
        }
    }

    func test_scan_givenNonExistentTarget_throwsError() {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("does-not-exist.pbxproj")
        XCTAssertThrowsError(try sut.scan(target: file)) { error in
            XCTAssertEqual(error as? RuleError, RuleError.targetFileDoesNotExist)
        }
    }

    func test_scan_givenTargetIsNotPbxproj_throwsErrors() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("/not-pbxproj.file")
        try file.overwrite("")
        XCTAssertThrowsError(try sut.scan(target: file)) { error in
            XCTAssertEqual(error as? RuleError, RuleError.targetFileIsNotPbxproj)
        }
        // cleanup
        do { try file.delete() } catch {}
    }

    func test_scan_givenProjectFileWithScript_returnsExpectedViolation() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("project.pbxproj")
        let data =
            """
            // !$*UTF8*$!
            shellScript = "# this is a shell script";
            """
        try file.overwrite(data)
        let report = try sut.scan(target: file)
        let expectedViolation = RuleViolation(
            rule: sut, file: file.fullPath, line: 2, extraMessage: nil, data: "# this is a shell script"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
        // cleanup
        try file.delete()
    }

    func test_scan_givenScriptWithLineBreak1_returnsExpectedViolation() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("project.pbxproj")
        let data =
            """
            // !$*UTF8*$!
            shellScript
            = "# newlines between label and value";
            """
        try file.overwrite(data)
        let report = try sut.scan(target: file)
        let expectedViolation = RuleViolation(
            rule: sut, file: file.fullPath, line: 2, extraMessage: nil, data: "# newlines between label and value"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
        // cleanup
        try file.delete()
    }

    func test_scan_givenScriptWithLineBreak2_returnsExpectedViolation() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("project.pbxproj")
        let data =
            """
            // !$*UTF8*$!
            shellScript =
            "# newlines between label and value";
            """
        try file.overwrite(data)
        let report = try sut.scan(target: file)
        let expectedViolation = RuleViolation(
            rule: sut, file: file.fullPath, line: 2, extraMessage: nil, data: "# newlines between label and value"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
        // cleanup
        try file.delete()
    }

    func test_scan_givenScriptWithLineBreak3_returnsExpectedViolation() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("project.pbxproj")
        let data =
            """
            // !$*UTF8*$!
            shellScript = "
            # newlines between label and value";
            """
        try file.overwrite(data)
        let report = try sut.scan(target: file)
        let expectedViolation = RuleViolation(
            rule: sut, file: file.fullPath, line: 2, extraMessage: nil, data: "# newlines between label and value"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
        // cleanup
        try file.delete()
    }

    func test_scan_givenScriptWithLineBreak4_returnsExpectedViolation() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("project.pbxproj")
        let data =
            """
            // !$*UTF8*$!
            shellScript = "# newlines between label and value
            ";
            """
        try file.overwrite(data)
        let report = try sut.scan(target: file)
        let expectedViolation = RuleViolation(
            rule: sut, file: file.fullPath, line: 2, extraMessage: nil, data: "# newlines between label and value"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
        // cleanup
        try file.delete()
    }

    func test_scan_givenScriptWithLineBreakAfterEndQuote_returnsExpectedViolation() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("project.pbxproj")
        let data =
            """
            // !$*UTF8*$!
            shellScript = "# newlines between label and value"
            ;
            """
        try file.overwrite(data)
        let report = try sut.scan(target: file)
        let expectedViolation = RuleViolation(
            rule: sut, file: file.fullPath, line: 2, extraMessage: nil, data: "# newlines between label and value"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
        // cleanup
        try file.delete()
    }

    func test_scan_givenScriptWithLineBreaksEverywhere6_returnsExpectedViolation() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("project.pbxproj")
        let data =
            """
            // !$*UTF8*$!
            shellScript
            =
            "
            # newlines between
            # label and value
            "
            ;
            """
        try file.overwrite(data)
        let report = try sut.scan(target: file)
        let expectedViolation = RuleViolation(
            rule: sut, file: file.fullPath, line: 2, extraMessage: nil, data: "# newlines between\n# label and value"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
        // cleanup
        try file.delete()
    }

    func test_scan_givenScriptWithBlankLine_returnsExpectedViolation() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("project.pbxproj")
        let data =
            """
            // !$*UTF8*$!

            shellScript
            =
            "
            # newlines between

            # label and value
            "
            ;
            """
        try file.overwrite(data)
        let report = try sut.scan(target: file)
        let expectedViolation = RuleViolation(
            rule: sut, file: file.fullPath, line: 3, extraMessage: nil, data: "# newlines between\n\n# label and value"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
        // cleanup
        try file.delete()
    }

    func test_scan_givenFileWithBlankLines_reportsTheCorrectLineNumber() throws {
        let sut = EmbeddedScriptsRule()
        let file = SamplesHelpers.pbxprojAccessor
        let report = try sut.scan(target: file)
        let expectedViolation = RuleViolation(
            rule: sut, file: file.fullPath, line: 5, extraMessage: nil, data: "# this is a shell script"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
    }

    func test_scan_givenTwoScripts_returnsExpectedReport() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("project.pbxproj")
        let data =
            """
            // !$*UTF8*$!
            shellScript = "script 1";
            stuff;
            shellScript = "script 2";
            """
        try file.overwrite(data)
        let report = try sut.scan(target: file)
        let expectedViolation1 = RuleViolation(
            rule: EmbeddedScriptsRule(),
            file: file.fullPath,
            line: 2,
            extraMessage: nil,
            data: "script 1"
        )
        let expectedViolation2 = RuleViolation(
            rule: EmbeddedScriptsRule(),
            file: file.fullPath,
            line: 4,
            extraMessage: nil,
            data: "script 2"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation1)
        expectedReport.append(expectedViolation2)
        XCTAssertEqual(report[1].data, "script 2")
        XCTAssertEqual(report, expectedReport)
        // cleanup
        try file.delete()
    }

    func test_scan_whenCalledTwice_createsAFreshReportEachTime() throws {
        let sut = EmbeddedScriptsRule()
        let file = self.testFile("project.pbxproj")
        let data =
            """
            // !$*UTF8*$!
            shellScript = "script";
            """
        try file.overwrite(data)
        let firstReport = try sut.scan(target: file)
        let secondReport = try sut.scan(target: file)
        let expectedViolation = RuleViolation(
            rule: EmbeddedScriptsRule(),
            file: file.fullPath,
            line: 2,
            extraMessage: nil,
            data: "script"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(firstReport, expectedReport)
        XCTAssertEqual(secondReport.count, 1)
        XCTAssertEqual(secondReport.first?.line, 2)
        XCTAssertEqual(secondReport.first?.data, "script")
        XCTAssertEqual(secondReport.first, expectedViolation)
        XCTAssertEqual(secondReport, expectedReport)
        // cleanup
        try file.delete()
    }

    // MARK: - equals()

    func test_equals_givenSameTypeOfRule_returnsTruet() throws {
        let sut = EmbeddedScriptsRule()
        XCTAssertTrue(sut.equals(EmbeddedScriptsRule()))
    }

    func test_equals_givenOtherTypeOfRule_returnsFalse() throws {
        let sut = EmbeddedScriptsRule()
        let other = MockRule()
        XCTAssertFalse(sut.equals(other))
    }

    // target is not a valid pbxproj

    // has no scripts

    // has a script

    // has multiple scripts

    // MARK: - Test Helpers

    /// Get a FileAccessor in the test directory.
    /// - Parameter name: The filename.
    /// - Returns: A `FileAccessor` for the file, in the test directory (caches directory).
    private func testFile(_ name: String = "test") -> FileAccessor {
        let url = self.testDirURL.appendingPathComponent(name)
        return FileAccessor(url: url)
    }

    /// URL for the user’s Cache directory.
    private let testDirURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).last!
}
