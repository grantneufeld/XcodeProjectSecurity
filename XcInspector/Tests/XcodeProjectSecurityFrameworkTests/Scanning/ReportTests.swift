// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class ReportTests: XCTestCase {
    // MARK: .timestamp

    func test_timestamp_defaultsToBeginningOfUnixEpoch() throws {
        let sut = Report()
        XCTAssertEqual(sut.timestamp, Date(timeIntervalSince1970: 0))
    }

    // MARK: .isEmpty

    func test_isEmpty_defaultsToTrue() throws {
        let sut = Report()
        XCTAssertTrue(sut.isEmpty)
    }

    func test_isEmpty_givenElementInCollection_returnsFalse() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "element"))
        XCTAssertFalse(sut.isEmpty)
    }

    // MARK: .startIndex

    func test_startIndex_givenEmpty_returnsZero() throws {
        let sut = Report()
        XCTAssertEqual(sut.startIndex, 0)
    }

    func test_startIndex_givenNotEmpty_returnsZero() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: ExecutableFilesRule(), file: "startIndex.sh"))
        XCTAssertEqual(sut.startIndex, 0)
    }

    // MARK: .endIndex

    func test_endIndex_givenEmpty_returnsZero() throws {
        let sut = Report()
        XCTAssertEqual(sut.endIndex, 0)
    }

    func test_endIndex_givenOneViolation_returnsOne() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: ExecutableFilesRule(), file: "endIndex.sh"))
        XCTAssertEqual(sut.endIndex, 1)
    }

    // MARK: .count

    func test_count_defaultsToZero() throws {
        let sut = Report()
        XCTAssertEqual(sut.count, 0)
    }

    func test_count_givenElementInCollection_returnsOne() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "one"))
        XCTAssertEqual(sut.count, 1)
    }

    // MARK: - ==()

    func test_equals_givenBothEmpty_returnsTrue() throws {
        let sut = Report()
        let other = Report()
        XCTAssertEqual(sut, other)
    }

    func test_equals_givenFirstHasViolation_returnsFalse() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: UserScriptSandboxingRule(), file: "unequal.pbxproj", line: 23))
        let other = Report()
        XCTAssertNotEqual(sut, other)
    }

    func test_equals_givenHaveSlightlyDifferentViolations_returnsFalse() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: UserScriptSandboxingRule(), file: "slightly-different.pbxproj", line: 33))
        let other = Report()
        other.append(RuleViolation(rule: UserScriptSandboxingRule(), file: "slightly-different.pbxproj", line: 34))
        XCTAssertNotEqual(sut, other)
    }

    func test_equals_givenHaveTheSaneViolations_returnsTrue() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "identical.pbxproj", line: 45))
        let other = Report()
        other.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "identical.pbxproj", line: 45))
        XCTAssertEqual(sut, other)
    }

    // MARK: - +=()

    func test_plusAssign_givenBothEmpty_resultIsEmpty() throws {
        let sut = Report()
        let other = Report()
        sut += other
        XCTAssertTrue(sut.isEmpty)
    }

    func test_plusAssign_givenOtherIsEmpty_resultIsOriginal() throws {
        let sut = Report()
        let violation = RuleViolation(rule: ExecutableFilesRule(), file: "other-is-empty.sh")
        sut.append(violation)
        let other = Report()
        sut += other
        let expectedReport = Report()
        expectedReport.append(violation)
        XCTAssertEqual(sut, expectedReport)
    }

    func test_plusAssign_givenEmptyButOtherHasViolation_resultIsOthersViolation() throws {
        let sut = Report()
        let other = Report()
        let violation = RuleViolation(rule: UserScriptSandboxingRule(), file: "others-violation.sh")
        other.append(violation)
        sut += other
        let expectedReport = Report()
        expectedReport.append(violation)
        XCTAssertEqual(sut, expectedReport)
    }

    func test_plusAssign_givenBothHaveViolation_resultIsJoinedViolations() throws {
        let sut = Report()
        let violation1 = RuleViolation(rule: EmbeddedScriptsRule(), file: "first-violation.pbxproj", line: 1)
        sut.append(violation1)
        let other = Report()
        let violation2 = RuleViolation(rule: UserScriptSandboxingRule(), file: "second-violation.sh", line: 2)
        other.append(violation2)
        sut += other
        let expectedReport = Report()
        expectedReport.append(violation1)
        expectedReport.append(violation2)
        XCTAssertEqual(sut, expectedReport)
    }

    // MARK: - Subscript

    func test_subscript_givenOneViolation_indexZero_returnsTheViolation() throws {
        let sut = Report()
        let violation = RuleViolation(rule: EmbeddedScriptsRule(), file: "project.pbxproj")
        sut.append(violation)
        let result = sut[0]
        XCTAssertEqual(result, violation)
    }

    // MARK: append()

    func test_append_givenHasViolationAlready_addsTheNewViolation() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "first"))
        let violation = RuleViolation(rule: ExecutableFilesRule(), file: "second")
        sut.append(violation)
        XCTAssertEqual(sut[1], violation)
    }

    // MARK: - Sequence

    func test_sequence_givenMultipleElements_loopsThroughEachElement() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "first"))
        sut.append(RuleViolation(rule: ExecutableFilesRule(), file: "second"))
        var filenames: [String] = []
        for violation in sut {
            filenames.append(violation.file)
        }
        XCTAssertEqual(filenames, ["first", "second"])
    }

    // MARK: - plainText()

    func test_plainText_givenNoViolations_returnsEmptyString() throws {
        let sut = Report()
        let result = sut.plainText()
        XCTAssertTrue(result.isEmpty)
    }

    func test_plainText_givenAViolation_returnsTextDescribingTheViolation() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "report", line: 3, data: "#!/bin/sh"))
        let result = sut.plainText()
        XCTAssertEqual(result, "report:3:warning:Found an embedded script in the pbxproj file.\n")
    }

    func test_plainText_givenMultipleViolations_returnsExpectedText() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: UserScriptSandboxingRule(), file: "project.pbxproj", line: 42))
        sut.append(RuleViolation(rule: ExecutableFilesRule(), file: "hacking.sh"))
        let result = sut.plainText()
        var expectedText = "project.pbxproj:42:warning:User Script Sandboxing flag not set in project.\n"
        expectedText.append("hacking.sh:warning:Found an executable file in the project directory.\n")
        XCTAssertEqual(
            result,
            expectedText
        )
    }

    func test_plainText_givenEmbeddedScriptViolation_andDetailsFlag_includesTheScript() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "project.pbxproj", line: 123, data: "The Script!"))
        let result = sut.plainText(includeDetails: true)
        var expectedText = "project.pbxproj:123:warning:Found an embedded script in the pbxproj file.\n"
        expectedText.append("    The Script!")
        expectedText.append("\n")
        XCTAssertEqual(result, expectedText)
    }

    func test_plainText_givenMultilineEmbeddedScript_andDetailsFlag_properlyIndentsScript() throws {
        let sut = Report()
        sut.append(
            RuleViolation(
                rule: EmbeddedScriptsRule(),
                file: "project.pbxproj",
                line: 123,
                data: "The Script!\n\nHas multiple lines!"
            )
        )
        let result = sut.plainText(includeDetails: true)
        var expectedText = "project.pbxproj:123:warning:Found an embedded script in the pbxproj file.\n"
        expectedText.append("    The Script!\n")
        expectedText.append("    \n")
        expectedText.append("    Has multiple lines!")
        expectedText.append("\n")
        XCTAssertEqual(result, expectedText)
    }

    func test_plainText_givenEmbeddedScriptWithSlashEncoding_andDetailsFlag_decodesScript() throws {
        let sut = Report()
        sut.append(
            RuleViolation(
                rule: EmbeddedScriptsRule(),
                file: "project.pbxproj",
                line: 123,
                data: "Script\\nhas \\\"\\\\-encoded\\\" chars"
            )
        )
        let result = sut.plainText(includeDetails: true)
        var expectedText = "project.pbxproj:123:warning:Found an embedded script in the pbxproj file.\n"
        expectedText.append("    Script\n")

        expectedText.append("    has \"\\-encoded\" chars")
        expectedText.append("\n")
        XCTAssertEqual(result, expectedText)
    }

    // MARK: - plainTextStats()

    func test_plainTextStats_givenEmptyReport_returnsZeroStats() throws {
        let sut = Report()
        let result = sut.plainTextStats()
        XCTAssertEqual(result, "Found 0 warnings.")
    }

    func test_plainTextStats_givenOneViolation_returnsOneStats() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: ExecutableFilesRule(), file: "", line: 1))
        let result = sut.plainTextStats()
        XCTAssertEqual(result, "Found 1 warning.")
    }

    func test_plainTextStats_givenTwoViolations_returnsTwoStats() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: ExecutableFilesRule(), file: "", line: 1))
        sut.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "", line: 2))
        let result = sut.plainTextStats()
        XCTAssertEqual(result, "Found 2 warnings.")
    }

    // MARK: - save()

    func test_save_givenDestinationAccessor_createsTheReportFile() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "project.pbxproj", line: 123))
        let fileURL = URL.cachesDirectory.appendingPathComponent("test_save.txt")
        let accessor = FileAccessor(url: fileURL)
        try sut.save(accessor)
        let fileContents = try accessor.read()
        XCTAssertEqual(fileContents, "project.pbxproj:123:warning:\(EmbeddedScriptsRule().message)\n")
        // cleanup
        try accessor.delete()
    }

    func test_save_givenFileExistsAtDestination_overwritesTheFile() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: ExecutableFilesRule(), file: "script.sh"))
        let fileURL = URL.cachesDirectory.appendingPathComponent("test_overwrite.txt")
        let accessor = FileAccessor(url: fileURL)
        try accessor.overwrite("This should be overwritten.")
        try sut.save(accessor)
        let fileContents = try accessor.read()
        XCTAssertEqual(fileContents, "script.sh:warning:\(ExecutableFilesRule().message)\n")
        // cleanup
        try accessor.delete()
    }

    // file permissions block save

    func test_save_givenDestinationIsNotWritable_throwsError() throws {
        let sut = Report()
        sut.append(RuleViolation(rule: UserScriptSandboxingRule(), file: "not_writable.pbxproj"))
        let fileURL = URL(fileURLWithPath: "/cannot_write.txt")
        let accessor = FileAccessor(url: fileURL)
        XCTAssertThrowsError(try sut.save(accessor))
    }
}
