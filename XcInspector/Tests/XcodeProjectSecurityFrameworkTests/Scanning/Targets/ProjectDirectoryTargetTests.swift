// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class ProjectDirectoryTargetTests: XCTestCase {
    func test_scan_givenAFileAccessor_throwsError() throws {
        let sut = ProjectDirectoryTarget(accessor: SamplesHelpers.nonProjectReadmeAccessor)
        XCTAssertThrowsError(try sut.scan()) { error in
            XCTAssertEqual(error as? AccessorError, .notADirectory)
        }
    }

    func test_scan_givenProjectDirectory_findsEmbeddedScriptsViolation() throws {
        XCTAssertTrue(SamplesHelpers.testProjectAccessor.exists)
        let sut = ProjectDirectoryTarget(accessor: SamplesHelpers.testProjectAccessor)
        let report = try sut.scan()
        let expectedViolation = RuleViolation(
            rule: EmbeddedScriptsRule(),
            file: SamplesHelpers.pbxprojURL.relativePath,
            line: 5,
            extraMessage: nil,
            data: "# this is a shell script"
        )
        XCTAssertTrue(report.contains(expectedViolation))
    }

    func test_scan_givenProjectDirectory_getsExecutableFileRuleViolation() throws {
        let sut = ProjectDirectoryTarget(accessor: SamplesHelpers.testProjectAccessor)
        let report = try sut.scan()
        let expectedViolation = RuleViolation(
            rule: ExecutableFilesRule(),
            file: SamplesHelpers.executableURL.relativePath,
            line: nil,
            extraMessage: nil,
            data: nil
        )
        XCTAssertTrue(report.contains(expectedViolation))
    }
}
