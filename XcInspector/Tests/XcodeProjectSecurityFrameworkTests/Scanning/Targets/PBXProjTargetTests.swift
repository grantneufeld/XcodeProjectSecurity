// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class PBXProjTargetTests: XCTestCase {
    func test_scan_givenDirectoryAccessor_throwsError() throws {
        let accessor = SamplesHelpers.testProjectAccessor
        let sut = PBXProjTarget(accessor: accessor)
        XCTAssertThrowsError(try sut.scan()) { error in
            XCTAssertEqual(error as? AccessorError, .notAFile)
        }
    }

    func test_scan_givenFileWithEmbeddedScript_returnsRuleViolationInArray() throws {
        let accessor = SamplesHelpers.pbxprojAccessor
        let sut = PBXProjTarget(accessor: accessor)
        let report = try sut.scan()
        let expectedViolation = RuleViolation(
            rule: EmbeddedScriptsRule(),
            file: accessor.fullPath,
            line: 5,
            extraMessage: nil,
            data: "# this is a shell script"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
    }

    func test_scan_givenFileWithNoUserScriptSandboxing_returnsRuleViolationInArray() throws {
        let accessor = SamplesHelpers.sandboxFlagMultiplePbxprojAccessor
        let sut = PBXProjTarget(accessor: accessor)
        let report = try sut.scan()
        let expectedViolation1 = RuleViolation(
            rule: UserScriptSandboxingRule(),
            file: accessor.fullPath,
            line: 3
        )
        let expectedViolation2 = RuleViolation(
            rule: UserScriptSandboxingRule(),
            file: accessor.fullPath,
            line: 5
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation1)
        expectedReport.append(expectedViolation2)
        XCTAssertEqual(report.plainText(), expectedReport.plainText())
    }
}
