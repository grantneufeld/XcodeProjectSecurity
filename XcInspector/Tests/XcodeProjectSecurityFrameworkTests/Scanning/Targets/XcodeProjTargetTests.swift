// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class XcodeProjTargetTests: XCTestCase {
    func test_scan_goesThroughAllSubFilesAndScansTargets() throws {
        let sut = XcodeProjTarget(accessor: SamplesHelpers.xcodeprojAccessor)
        let report = try sut.scan()
        let expectedViolation = RuleViolation(
            rule: EmbeddedScriptsRule(),
            file: SamplesHelpers.pbxprojURL.relativePath,
            line: 5,
            extraMessage: nil,
            data: "# this is a shell script"
        )
        let expectedReport = Report()
        expectedReport.append(expectedViolation)
        XCTAssertEqual(report, expectedReport)
    }
}
