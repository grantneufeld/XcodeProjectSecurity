// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XcodeProjectSecurityFramework
import XCTest

final class ScannedProjectsTests: XCTestCase {
    // MARK: - Collection

    // MARK: .count

    func test_count_defaultsToZero() throws {
        let sut = ScannedProjects()
        XCTAssertEqual(sut.count, 0)
    }

    // MARK: .startIndex

    func test_startIndex_defaultsToZero() throws {
        let sut = ScannedProjects()
        XCTAssertEqual(sut.startIndex, 0)
    }

    func test_startIndex_givenItemInCollection_isStillZero() throws {
        let sut = ScannedProjects()
        let project = Project(accessor: SamplesHelpers.testProjectAccessor)
        sut.append(project)
        XCTAssertEqual(sut.startIndex, 0)
    }

    // MARK: .endIndex

    func test_endIndex_defaultsToZero() throws {
        let sut = ScannedProjects()
        XCTAssertEqual(sut.endIndex, 0)
    }

    func test_endIndex_givenItemInCollection_returnsOne() throws {
        let sut = ScannedProjects()
        let project = Project(accessor: SamplesHelpers.testProjectAccessor)
        sut.append(project)
        XCTAssertEqual(sut.endIndex, 1)
    }

    // MARK: index()

    func test_index_whenEmpty_givenZeroIndex_returnsOne() throws {
        let sut = ScannedProjects()
        let result = sut.index(after: 0)
        XCTAssertEqual(result, 1)
    }

    func test_index_givenItemInCollection_givenZeroIndex_returnsOne() throws {
        let sut = ScannedProjects()
        let project = Project(accessor: SamplesHelpers.testProjectAccessor)
        sut.append(project)
        let result = sut.index(after: 0)
        XCTAssertEqual(result, 1)
    }

    // MARK: append()

    func test_append_givenValidAccessor_increasesTheCountByOne() throws {
        let sut = ScannedProjects()
        XCTAssertEqual(sut.count, 0)
        let project = Project(accessor: SamplesHelpers.testProjectAccessor)
        sut.append(project)
        XCTAssertEqual(sut.count, 1)
    }

    func test_append_givenExistingAccessorInCollection_increasesTheCountToTwo() throws {
        let sut = ScannedProjects()
        XCTAssertEqual(sut.count, 0)
        let project1 = Project(accessor: SamplesHelpers.testProjectAccessor)
        sut.append(project1)
        let project2 = Project(accessor: SamplesHelpers.executableAccessor)
        sut.append(project2)
        XCTAssertEqual(sut.count, 2)
    }

    func test_append_givenExistingAccessorInCollection_hasBothAccessors() throws {
        let sut = ScannedProjects()
        let project1 = Project(accessor: SamplesHelpers.testProjectAccessor)
        sut.append(project1)
        let project2 = Project(accessor: SamplesHelpers.executableAccessor)
        sut.append(project2)
        let first = sut[0]
        let second = sut[1]
        XCTAssertEqual(first, project1)
        XCTAssertEqual(second, project2)
    }

    // MARK: subscript

    func test_subscript_givenAValidAccessorInCollection_returnsTheAccessor() throws {
        let sut = ScannedProjects()
        let project = Project(accessor: SamplesHelpers.testProjectAccessor)
        sut.append(project)
        let result = sut[0]
        XCTAssertEqual(result, project)
    }

    // MARK: - Sequence
}
