// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation
import XcodeProjectSecurityFramework

/// Provide consistent access to the test Samples directory.
enum SamplesHelpers {
    static var nonProjectPath: String { "NonProject" }
    static var nonProjectReadmePath: String { "\(nonProjectPath)/README.md" }
    static var testProjectPath: String { "TestProject" }
    static var xcodeprojPath: String { "\(testProjectPath)/TestProject.xcodeproj" }
    static var pbxprojPath: String { "\(xcodeprojPath)/project.pbxproj" }
    static var executablePath: String { "\(testProjectPath)/executable.sh" }
    static var sandboxPath: String { "UserSandbox" }
    static var sandboxMissingPath: String { "\(sandboxPath)/MissingFlag" }
    static var sandboxMissingXcodeProjPath: String { "\(sandboxMissingPath)/MissingFlag.xcodeproj" }
    static var sandboxMissingPbxprojPath: String { "\(sandboxMissingXcodeProjPath)/project.pbxproj" }
    static var sandboxFlagYesPath: String { "\(sandboxPath)/FlagYes" }
    static var sandboxFlagYesXcodeProjPath: String { "\(sandboxFlagYesPath)/FlagYes.xcodeproj" }
    static var sandboxFlagYesPbxprojPath: String { "\(sandboxFlagYesXcodeProjPath)/project.pbxproj" }
    static var sandboxFlagNoPath: String { "\(sandboxPath)/FlagNo" }
    static var sandboxFlagNoXcodeProjPath: String { "\(sandboxFlagNoPath)/FlagNo.xcodeproj" }
    static var sandboxFlagNoPbxprojPath: String { "\(sandboxFlagNoXcodeProjPath)/project.pbxproj" }
    // Flag Multiple:
    static var sandboxFlagMultiplePath: String { "\(sandboxPath)/FlagMultiple" }
    static var sandboxFlagMultipleXcodeProjPath: String { "\(sandboxFlagMultiplePath)/FlagMultiple.xcodeproj" }
    static var sandboxFlagMultiplePbxprojPath: String { "\(sandboxFlagMultipleXcodeProjPath)/project.pbxproj" }

    static var nonProjectURL: URL { sampleUrl(for: nonProjectPath) }
    static var nonProjectReadmeURL: URL { sampleUrl(for: nonProjectReadmePath) }
    static var testProjectURL: URL { sampleUrl(for: testProjectPath) }
    static var xcodeprojURL: URL { sampleUrl(for: xcodeprojPath) }
    static var pbxprojURL: URL { sampleUrl(for: pbxprojPath) }
    static var executableURL: URL { sampleUrl(for: executablePath) }
    static var sandboxMissingPbxprojURL: URL { sampleUrl(for: sandboxMissingPbxprojPath) }
    static var sandboxFlagYesPbxprojURL: URL { sampleUrl(for: sandboxFlagYesPbxprojPath) }
    static var sandboxFlagNoPbxprojURL: URL { sampleUrl(for: sandboxFlagNoPbxprojPath) }
    static var sandboxFlagMultiplePbxprojURL: URL { sampleUrl(for: sandboxFlagMultiplePbxprojPath) }

    static var executableAccessor: FileAccessor {
        let url = sampleUrl(for: executablePath)
        return FileAccessor(url: url)
    }
    static var nonProjectAccessor: DirectoryAccessor {
        let url = sampleUrl(for: nonProjectPath)
        return DirectoryAccessor(url: url)
    }
    static var nonProjectReadmeAccessor: FileAccessor {
        let url = sampleUrl(for: nonProjectReadmePath)
        return FileAccessor(url: url)
    }
    static var testProjectAccessor: DirectoryAccessor {
        let url = sampleUrl(for: testProjectPath)
        return DirectoryAccessor(url: url)
    }
    static var xcodeprojAccessor: DirectoryAccessor {
        let url = sampleUrl(for: xcodeprojPath)
        return DirectoryAccessor(url: url)
    }
    static var pbxprojAccessor: FileAccessor {
        let url = sampleUrl(for: pbxprojPath)
        return FileAccessor(url: url)
    }
    static var sandboxMissingPbxproj: FileAccessor {
        let url = sampleUrl(for: sandboxMissingPbxprojPath)
        return FileAccessor(url: url)
    }
    static var sandboxFlagYesPbxproj: FileAccessor {
        let url = sampleUrl(for: sandboxFlagYesPbxprojPath)
        return FileAccessor(url: url)
    }
    static var sandboxFlagNoPbxproj: FileAccessor {
        let url = sampleUrl(for: sandboxFlagNoPbxprojPath)
        return FileAccessor(url: url)
    }
    static var sandboxFlagMultiplePbxprojAccessor: FileAccessor {
        let url = sampleUrl(for: sandboxFlagMultiplePbxprojPath)
        return FileAccessor(url: url)
    }

    /// Get a URL for a path within the Samples directory.
    /// - Parameter sample: A path fragment, relative to the Samples directory.
    /// - Returns: `URL` for the resource defined by the path.
    static func sampleUrl(for sample: String) -> URL {
        samplesDirectory().appendingPathComponent(sample)
    }

    /// Do not pass in a parameter.
    /// - Parameter path: Do not set this—let the default be used.
    /// - Returns: An `URL` for the “Samples” directory.
    static func samplesDirectory(path: String = #file) -> URL {
        let url = URL(fileURLWithPath: path)
        let testsDir = url
            .deletingLastPathComponent() // -> TestHelpers
            .deletingLastPathComponent() // -> Tests
            .deletingLastPathComponent() // -> XcInspector
            .deletingLastPathComponent() // -> XcodeProjectSecurity
        return testsDir.appendingPathComponent("TestSamples")
    }

    // MARK: - Violation Counts

    static let testProjectViolationCount: Int = 2
}
