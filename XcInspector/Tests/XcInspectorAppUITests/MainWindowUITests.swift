// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import XCTest

final class MainWindowUITests: XCTestCase {
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    // MARK: - Projects Column

    // MARK: ContentUnavailableView

    func test_contentUnavilable_givenNoReports_hasExpectedText() throws {
        let app = XCUIApplication()
        app.launch()
        let contentUnavailable = app.descendants(matching: .any)["projectsUnavailable"]
        XCTAssertEqual(contentUnavailable.value as? String, "No Projects Scanned")
    }

    // MARK: ScanButtonView

    func test_projectsScanButton_hasCorrectTitle() throws {
        let app = XCUIApplication()
        app.launch()
        let scanButton = app.buttons["projectsScan"]
        let buttonLabel = scanButton.label
        XCTAssertEqual(buttonLabel, "Scan")
    }

    func test_projectsScanButton_triggersFileImport() throws {
        let app = XCUIApplication()
        app.launch()
        let scanButton = app.buttons["projectsScan"]
        addUIInterruptionMonitor(
            withDescription: "This describes stuff about what we’re trying to handle."
        ) { element in
            // how to select directory???
            let openButton = element.buttons["Open"]
            openButton.tap()
            return true
        }
        scanButton.tap()
        let resultButton = app.buttons["projectsScan"]
        XCTAssertEqual(resultButton.label, "Scan")
    }

    // MARK: - Report Column

    // MARK: ContentUnvailableView

    func test_reportContentUnavilable_givenNoReports_hasExpectedText() throws {
        let app = XCUIApplication()
        app.launch()
        let contentUnavailable = app.descendants(matching: .any)["reportUnavailable"]
        XCTAssertTrue(contentUnavailable.exists)
        XCTAssertEqual(contentUnavailable.value as? String, "No Report")
    }

    // MARK: ScanButtonView

    func test_reportScanButton_hasCorrectTitle() throws {
        let app = XCUIApplication()
        app.launch()
        let scanButton = app.buttons["reportScan"]
        let buttonLabel = scanButton.label
        XCTAssertEqual(buttonLabel, "Scan")
    }

    func test_reportScanButton_triggersFileImport() throws {
        let app = XCUIApplication()
        app.launch()
        let scanButton = app.buttons["reportScan"]
        addUIInterruptionMonitor(
            withDescription: "This describes stuff about what we’re trying to handle."
        ) { element in
            let openButton = element.buttons["Open"]
            openButton.tap()
            return true
        }
        scanButton.tap()
        let resultButton = app.buttons["reportScan"]
        XCTAssertEqual(resultButton.label, "Scan")
    }
}
