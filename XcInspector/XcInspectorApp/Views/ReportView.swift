// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import SwiftUI
import XcodeProjectSecurityFramework

struct ReportView: View {
    var report: Report

    var body: some View {
        VStack(alignment: .leading) {
            List(report) { violation in
                HStack(alignment: .top) {
                    IconSymbol.warning.image
                        .foregroundStyle(.yellow)
                    VStack(alignment: .leading) {
                        Text(violation.rule.message)
                            .font(.headline)
                        Text("File: \(violation.file)")
                        if let line = violation.line {
                            Text("Line: \(String(line))")
                        }
                        if let data = violation.data {
                            Text(data.decodeSlashEncodedCharacters.trimmingCharacters(in: .whitespacesAndNewlines))
                                .font(.monospaced(.body)())
                                .padding()
                                .background(Color(NSColor.windowBackgroundColor))
                                .border(.gray)
                                .padding(.horizontal)
                        }
                    }
                }
            }
            Text("Found ^[\(report.count) warning](inflect: true).")
                .padding()
                .font(.title)
        }
        .toolbar {
            ToolbarItem(placement: .automatic) {
                ShareLink(
                    item: self.report.plainText(),
                    subject: Text("XcInspector Report for \(self.report.timestamp)")
                )
            }
        }
    }
}

#Preview {
    let report = Report()
    report.append(RuleViolation(rule: EmbeddedScriptsRule(), file: "preview"))
    return ReportView(report: report)
}
