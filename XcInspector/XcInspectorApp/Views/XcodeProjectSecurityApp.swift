// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import SwiftUI
import XcodeProjectSecurityFramework

@main
struct XcodeProjectSecurityApp: App {
    @State private var model = MainController()

    var body: some Scene {
        WindowGroup {
            MainWindow(model: $model)
                .fileImporter(isPresented: self.$model.openAction, allowedContentTypes: [.directory]) { result in
                    switch result {
                    case .success(let url):
                        self.model.open(url)

                    case .failure(let error):
                        self.model.reportError(error: error)
                    }
                }
                .alert( "Error Encountered", isPresented: self.$model.hasError) {
                    Button("OK") { self.model.hideError() }
                } message: {
                    Text(self.model.error?.localizedDescription ?? "")
                }
                .onOpenURL { url in
                    self.model.open(url)
                }
        }
        .commands {
            CommandGroup(before: CommandGroupPlacement.newItem) {
                Button("Open…") {
                    self.model.openAction = true
                }
                .keyboardShortcut("o")
            }
        }
    }
}
