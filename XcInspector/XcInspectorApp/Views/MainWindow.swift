// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import SwiftUI

struct MainWindow: View {
    @Binding var model: MainController

    var body: some View {
        NavigationSplitView {
            ProjectsListScreen(model: $model)
        } detail: {
            ReportScreen(model: $model)
                .navigationTitle(model.project?.accessor.filename ?? "")
        }
    }
}

#Preview {
    @State var model = MainController()
    return MainWindow(model: $model)
}
