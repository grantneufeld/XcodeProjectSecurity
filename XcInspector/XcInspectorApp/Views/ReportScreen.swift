// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import SwiftUI

struct ReportScreen: View {
    @Binding var model: MainController

    var body: some View {
        if model.project?.report != nil {
            ReportView(report: model.project!.report!)
        } else {
            ContentUnavailableView("No Report", systemImage: IconSymbol.empty.rawValue)
                .accessibilityIdentifier("reportUnavailable")
            ScanButtonView(model: $model)
                .accessibilityIdentifier("reportScan")
        }
    }
}

#Preview {
    @State var model = MainController()
    return ReportScreen(model: $model)
}
