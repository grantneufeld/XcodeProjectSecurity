// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import SwiftUI
import XcodeProjectSecurityFramework

struct ProjectsListScreen: View {
    @Binding var model: MainController

    var body: some View {
        VStack {
            if self.model.projects.isEmpty {
                ContentUnavailableView("No Projects Scanned", systemImage: IconSymbol.empty.rawValue)
                    .accessibilityIdentifier("projectsUnavailable")
            } else {
                List(model.projects, selection: $model.project) { project in
                    ProjectListItemView(project: project)
                        .tag(project)
                }
                Spacer()
            }
            ScanButtonView(model: $model)
                .accessibilityIdentifier("projectsScan")
                .padding()
        }
    }
}

#Preview {
    @State var model = MainController()
    return ProjectsListScreen(model: $model)
}
