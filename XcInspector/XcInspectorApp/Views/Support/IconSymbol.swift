// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import SwiftUI
import XcodeProjectSecurityFramework

/// Used to keep track of the SF Symbol icons used in this app.
enum IconSymbol: String {
    /// When there are none of the item type (e.g., projects, reports).
    case empty = "square.dotted"
    /// A checkmark in a shield.
    case passed = "checkmark.shield.fill"
    /// An exclamation mark in a triangle.
    case warning = "exclamationmark.triangle.fill"

    /// Get a SwiftUI Image View of the icon.
    var image: Image {
        Image(systemName: self.rawValue)
    }
}
