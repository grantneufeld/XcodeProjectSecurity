// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import SwiftUI

struct ScanButtonView: View {
    @Binding var model: MainController

    var body: some View {
        Button("Scan") {
            self.model.openAction = true
        }
    }
}

#Preview {
    @State var model = MainController()
    return ScanButtonView(model: $model)
}
