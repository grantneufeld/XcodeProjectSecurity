// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import SwiftUI
import XcodeProjectSecurityFramework

struct ProjectListItemView: View {
    var project: Project

    var body: some View {
        HStack(alignment: .top) {
            self.icon
            VStack(alignment: .leading) {
                Text(project.accessor.filename)
                    .font(.headline)
                    .fontWeight(.bold)
                Text("^[\(project.report?.count ?? 0) warning](inflect: true) found")
                if let report = project.report {
                    Text(report.timestamp.formatted())
                        .font(.caption)
                }
            }
        }
        .help(project.accessor.fullPath)
    }

    var icon: some View {
        if project.report?.isEmpty ?? true {
            return IconSymbol.passed.image
                .foregroundStyle(.green)
        }
        return IconSymbol.warning.image
            .foregroundStyle(.yellow)
    }
}

#Preview {
    @State var project = Project(accessor: FileAccessor(url: URL(filePath: "")))
    return ProjectListItemView(project: project)
}
