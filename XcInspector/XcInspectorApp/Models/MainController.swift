// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation
import XcodeProjectSecurityFramework

enum MainError: Error {
    case noProject
    case noReport
}

/// Model backing the main app scene (`XcodeProjectSecurityApp`).
@Observable
final class MainController: ObservableObject {
    /// Error encountered that should be shown to user.
    var error: Error?
    /// Whether the error alert should be shown.
    var hasError = false
    /// Whether the open action (fileImporter) should be shown.
    var openAction = false
    /// Whether the save action (fileExporter) should be shown.
    var saveAction = false
    /// The most recently scanned project.
    var project: Project?
    /// The collection of projects that have been scanned.
    var projects = ScannedProjects()

    /// Call when an error is encountered that should be reported to the user.
    /// - Parameter error: The error that should be reported.
    func reportError(error: Error) {
        self.error = error
        self.hasError = true
    }

    /// Stop showing the error alert.
    func hideError() {
        self.hasError = false
    }

    /// Tries to open the resource at the given URL and run a scan of it for any rule violations.
    /// - Parameter url: A `URL` to a file or directory.
    func open(_ url: URL) {
        do {
            let runner = try ScanRunner(url)
            let report = try runner.scan()
            let project = Project(accessor: runner.accessor, report: report)
            self.project = project
            self.projects.append(project)
        } catch {
            self.reportError(error: ScanRunnerError.unrecognizedAccessor)
        }
    }

    /// Tries to save the active report, if there is one.
    /// - Parameter url: The destination file to save the report in.
    /// - Throws: If there is no project, or report, or if the report data failed to be written to the file.
    func save(_ url: URL) throws {
        guard let project else {
            throw MainError.noProject
        }
        guard let report = project.report else {
            throw MainError.noReport
        }
        let text = report.plainText()
        guard let textData = text.data(using: .utf8) else {
            throw FileAccessorError.failedToConvertTextToData
        }
        if let fileHandle = FileHandle(forWritingAtPath: url.path) {
            // file already exists; overwrite it
            defer { fileHandle.closeFile() }
            try fileHandle.write(contentsOf: textData)
            try fileHandle.truncate(atOffset: try fileHandle.offset())
        } else {
            // write new file
            try textData.write(to: url, options: .atomic)
        }
    }
}
