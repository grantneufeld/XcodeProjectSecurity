# XcInspector

This is the Xcode project that builds the XcInspector (Xcode Project security) tools.

- [Source Code Repository on GitLab](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity)

## Built Products

* `XcInspectorApp` is the macOS app for scanning Xcode projects for possible
  security issues.
* `CommandLineTool` is the command line tool version of XcInspector.
* `XcodeProjectSecurityFramework` is the code shared by the tools.

## Tests

With 3 main targets being built (so far), there are a number of different sets
of tests to cover:

* `XcodeProjectSecurityFrameworkTests` is the primary set of unit tests for
  all of the shared code.
* `XcInspectorAppTests` is the unit tests for the macOS App.
* `XcInspectorAppUITests` is the UI tests for the macOS App.
* `CommandLineInterfaceTests` is the UI tests for the XcInspector command line
  tool.

There is also the `TestHelpers` group, which contains source files included in
all of the test targets. The `TestHostApp` group contains a bare minimum app
for use by the unit tests that need to have entitlements granted. 

### Test Plans & Schemes

There are currently 5 schemes, most with corresponding test plans:

* `XcodeProjectSecurityFramework` builds and tests the shared code framework.
* `XcInspectorApp` builds and unit tests the macOS app.
* `CommandLineTool` builds and UI tests the XcInspector command line tool.
* `CommandLineToolRelease` builds the release version of the XcInspector
  command line tool. There is no test plan for this one.
* `TestEverything` runs every test suite.

### Test Sample Files

The sample files used in tests for this project are kept one directory above
this Xcode project’s directory (in `TestSamples`).

Normally, test sample files would be included in a project’s directory
(typically in a tests group). But, because our sample files include custom
(and sometimes deliberately malformed) Xcode project files, they can
interfere with Xcode’s operation when included inside the project directory. 

## Coding Practices

All contributions should be developed using:

- [Pair programming](https://en.wikipedia.org/wiki/Pair_programming), or
  [ensemble programming](https://en.wikipedia.org/wiki/Team_programming#Mob_programming)
  (generally using online voice chat and screen-sharing).
- [Test-Driven Development](https://en.wikipedia.org/wiki/Test-driven_development)
  (TDD).
- [Trunk-based development](https://trunkbaseddevelopment.com/).
  No branching; all pushed commits go on the `main` branch.

### Swift Language Practices

- Follow the [SwiftLint](https://github.com/realm/SwiftLint) rules,
  as configured for the project.
- Use
  [DocC](https://developer.apple.com/documentation/xcode/writing-symbol-documentation-in-your-source-files)
  format comments for most classes, structs, enums, properties,
  and functions (except unit test cases).
- Prefer protocols over class inheritance.

## Libraries Used

We are using Swift Package Manager to bring in external libraries.

- [swift-argument-parser]():
  Provides the `ArgumentParser` module used to provide the command line tool
  interface.
- [ViewInspector]():
  A module that helps with unit testing (not UI testing) SwiftUI Views.

---

## Legal

### DISCLAIMER

While this tool may help identify some possible security risks,
it can IN NO WAY GUARANTEE the safety of any particular Xcode project,
or any derivative product.

By using this software, you agree that you are solely responsible for ensuring
the security of any Xcode projects you use or share, and absolve all authors of,
contributors to, or distributors of, XcInspector of any responsibility or
liability.

### Copyright

Copyright ©2023-2024 Grant Neufeld, et.al.

### License

The Source Code of this project is made available under the terms of the
[Mozilla Public License, v. 2.0](https://mozilla.org/MPL/2.0/).

The [license](LICENSE) file is included with the project files.
