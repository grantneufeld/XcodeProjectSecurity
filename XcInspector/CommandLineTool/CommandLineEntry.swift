// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import ArgumentParser
import Foundation
import XcodeProjectSecurityFramework

enum CommandLineError: Error {
case fileDoesNotExist
}

/// Is the entry point for the command line tool.
@main
struct CommandLineEntry: ParsableCommand {
    @Flag(name: .shortAndLong, help: "Show statistics at the end of the report.")
    var stats = false

    @Flag(name: .shortAndLong, help: "Show expanded details with warnings.")
    var details = false

    /// The path to the Xcode project directory or project file.
    @Argument(help: "Path to Xcode project directory, or .xcodeproj file.")
    var filepath: String

    static var configuration = CommandConfiguration(
        commandName: "xcinspector",
        abstract: "XcInspector scans Xcode project files for possible security issues.",
        version: "0.1.0"
    )

    /// Called when the command line tool is invoked.
    mutating func run() throws {
        guard FileManager.default.fileExists(atPath: self.filepath) else {
            throw CommandLineError.fileDoesNotExist
        }
        let filepathURL = URL(fileURLWithPath: self.filepath)
        let absoluteURL = filepathURL.absoluteURL
        let runner = try ScanRunner(absoluteURL)
        let report = try runner.scan()
        if !report.isEmpty {
            print(report.plainText(includeDetails: self.details))
        }
        if self.stats {
            print(report.plainTextStats())
        }
    }
}
