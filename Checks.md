# XcInspector: Checks (aka “Rules”)

These are the security checks that are being considered to be implemented for this project.

## *.xcodeproj/project.pbxproj

### User Script Sandboxing off (Xcode >=15)

Warning if the sandboxing flag is off.

### Embedded scripts in project file

* Learn to recognize “normal” ones, like SwiftLint.
* Look for “risky” calls (file system, network).

### Input/Output Files & File Lists

Validate paths supplied to scripts to check that none go outside the `SRCROOT`
(Input Files, Input File Lists, Output Files, Output File Lists).

## Executable Files

Any files with the `+x` flag set.

## Script Files

Any files that could be run as scripts.

Maybe run those files against further checks that scan calls to the filesystem,
or network? Or other known “risky behaviour”?

## Pre-compiled Files 

Dynamic libraries, obj files, etc.

## Anything else?

If there are other checks you think would be beneficial to include in this project, please
[add the suggestion as an issue](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/-/issues).
