<!-- omit in toc -->
# Contributing to XcInspector

First off, thanks for taking the time to contribute! 💚

All types of contributions are encouraged and valued.
Please make sure to read the relevant section below,
before making your contribution.
That will make it a lot easier for the maintainers,
and smooth out the experience for all involved.

<!-- omit in toc -->
## Table of Contents

- [Code of Conduct](#code-of-conduct)
- [Have a Question?](#have-a-question)
- [How To Contribute](#how-to-contribute)
  - [Reporting Bugs](#reporting-bugs)
  - [Suggesting Enhancements](#suggesting-enhancements)
  - [Coding Practices](#coding-practices)
  - [Your First Code Contribution](#your-first-code-contribution)
  - [Improving The Documentation](#improving-the-documentation)
- [Styleguides](#styleguides)
  - [Commit Messages](#commit-messages)
- [Join The Project Team](#join-the-project-team)

## Code of Conduct

This project, and everyone participating in it, is governed by the
[XcInspector Code of Conduct](CODE_OF_CONDUCT.md).
By contributing, or otherwise participating, you are expected to uphold this
code. Please report unacceptable behavior to
[Grant Neufeld](https://gitlab.com/grantneufeld).

## Have a Question?

> If you want to ask a question, we assume that you have read the available
  [Documentation](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/-/wikis/home).

Before you ask a question, it is best to search for existing
[Issues](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/issues)
that might help you.
In case you have found a suitable issue and still need clarification,
you can write your question as a comment in that issue.
It is also advisable to search the internet for answers first.

If you then still feel the need to ask a question and need clarification,
we recommend the following:

- Open an [Issue](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/issues/new).
- Provide as much context as you can about what you're running into.
- Provide project and platform versions (Xcode, macOS, …).

## How To Contribute

> ### Legal Notice <!-- omit in toc -->
> When contributing to this project, you must agree that you have authored 100%
  of the content, that you have the necessary rights to the content and that the
  content you contribute may be provided under the [project license](LICENSE).

### Reporting Bugs

<!-- omit in toc -->
#### Before Submitting a Bug Report

A good bug report shouldn't leave others needing to chase you up for more
information. Therefore, we ask you to investigate carefully, collect information
and describe the issue in detail in your report. Please complete the following
steps in advance to help us fix any potential bug as fast as possible.

- Make sure that you are using the latest version.
- Determine if your bug is really a bug and not an error on your side
  e.g. using incompatible environment components/versions
  (Make sure that you have read the
  [documentation](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/-/wikis/home).
  If you are looking for support, you might want to check
  [this section](#have-a-question)).
- To see if other users have experienced (and potentially already solved) the
  same issue you are having, check if there is not already a bug report existing
  for your bug or error in the
  [bug tracker](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurityissues?q=label%3Abug).
- Also make sure to search the internet (including Stack Overflow) to see if
  users outside of the GitLab community have discussed the issue.
- Collect information about the bug:
  - Stack trace (Traceback).
  - OS, Platform and Version (macOS, x86, ARM).
  - Version of Xcode, runtime environment, depending on what seems relevant.
  - Can you reliably reproduce the issue? And can you also reproduce it with
    older versions?

<!-- omit in toc -->
#### How Do I Submit a Good Bug Report?

> You must never report security related issues, vulnerabilities or bugs
  including sensitive information to the issue tracker, or elsewhere in public.
  Instead sensitive bugs must be sent privately to
  [Grant Neufeld](https://gitlab.com/grantneufeld).

We use GitLab issues to track bugs and errors. If you run into an issue with
the project:

- Open an
  [Issue](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/issues/new).
- Explain the behavior you would expect and the actual behavior you observe.
- Please provide as much context as possible and describe the
  *reproduction steps* that someone else can follow to recreate the issue on
  their own. For good bug reports you should isolate the problem and create a
  reduced test case.
- Provide the information you collected in the previous section.

Once it's filed:

- The project team will label the issue accordingly.
- A team member will try to reproduce the issue with your provided steps.
  If there are no reproduction steps or no obvious way to reproduce the issue,
  the team will ask you for those steps and mark the issue as `needs-repro`.
  Bugs with the `needs-repro` tag will not be addressed until they are
  reproduced.
- If the team is able to reproduce the issue, it will be marked `confirmed`,
  as well as possibly other tags (such as `critical`), and the issue will be
  left to be [implemented by someone](#your-first-code-contribution).

### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for
XcInspector, **including completely new features and minor improvements
to existing functionality**.

<!-- omit in toc -->
#### Before Submitting an Enhancement

- Make sure that you are using the latest version.
- Read the
  [documentation](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/-/wikis/home)
  carefully and find out if the functionality is already covered, maybe by an
  individual configuration.
- Perform a
  [search](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/issues)
  to see if the enhancement has already been suggested. If it has, add a comment
  to the existing issue instead of opening a new one.
- Find out whether your idea fits with the scope and aims of the project.
  It's up to you to make a strong case to convince the rest of the team of
  the merits of this feature.

<!-- omit in toc -->
#### How Do I Submit a Good Enhancement Suggestion?

Enhancement suggestions are tracked as
[GitLab issues](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/issues).

- Use a **clear and descriptive title** for the issue to identify the
  suggestion.
- Provide a **step-by-step description of the suggested enhancement** in as
  many details as possible.
- **Describe the current behavior** and **explain which behavior you expected
  to see instead** and why. At this point you can also tell which alternatives
  do not work for you.

### Coding Practices

We require that all code contributions be developed using:

- [Pair programming](https://en.wikipedia.org/wiki/Pair_programming), or
  [ensemble/mob programming](https://en.wikipedia.org/wiki/Team_programming#Mob_programming)
  (generally using online voice chat and screen-sharing).

Additionally, where possible, code contributions will be developed using:

- [Test-Driven Development](https://en.wikipedia.org/wiki/Test-driven_development)
  (TDD).

Unfortunately, SwiftUI does not lend itself well to unit testing. So the
SwiftUI portions of the application code will instead have to rely on UI Tests.

Finally, we follow the practice of:

- [Trunk-based development](https://trunkbaseddevelopment.com/).
  No branching; all pushed commits go on the `main` branch.

Given that this is a security project, the first two of those are particularly
important. We don’t want any code in the project that hasn’t been worked on by
at least two people; and all code should have good, comprehensive, unit tests.

#### Code Coverage

While “code coverage” is not a measure of the completeness of tests,
having less than 100% coverage points to likely incomplete tests.
So, we aim for (not absolutely require) 100% coverage, where possible
(acknowledging that some tiny portions of code may be unreachable/untestable,
due to the structure of Apple’s frameworks).

<!-- omit in toc -->
#### Swift Language Practices

- Follow the [SwiftLint](https://github.com/realm/SwiftLint) rules,
  as configured for the project.
- Use
  [DocC](https://developer.apple.com/documentation/xcode/writing-symbol-documentation-in-your-source-files)
  format comments for most classes, structs, enums, properties,
  and functions (except test cases).
- Prefer protocols over class inheritance.

### Your First Code Contribution

<!-- omit in toc -->
#### Setting up your development environment

- macOS 14 (Sonoma), or later.
- Xcode 15 (it may work in Xcode 14, but that is untested).
- [SwiftLint](https://github.com/realm/SwiftLint) 0.55.1, or later.

Make sure that `swiftlint` in in your `PATH`, as defined by your
`~/.zshrc` file.

### Improving The Documentation

Please use the
[project wiki](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/-/wikis/home)
to add to, or improve, the documentation.

## Styleguides

### Commit Messages

Please list all participants for code produced using pair, or ensemble (mob),
programming.

They should be listed, one line each, in a block of “Co-authored-by:” lines at
the end of the commit message. Their email addresses should be the ones they
use for git commits submitted to GitLab/GitHub.

```git
The commit message subject line.

(... details about what was changed ...)

Co-authored-by: C. Ontributor <contributor@example.tld>
Co-authored-by: Anne Other <another@example.tld>
```

## Join The Project Team

We currently have a text channel for the project in the Swift Dev Chat Discord
(contact [Grant](https://gitlab.com/grantneufeld) if you need access). We also
have an online work session for the project there roughly once a week
(voice and screen sharing).

<!-- omit in toc -->
## Attribution

This guide started with a template from the **contributing-gen**.
[Make your own](https://github.com/bttger/contributing-gen)!
