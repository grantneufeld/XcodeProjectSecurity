# XcInspector: for Xcode Project Security

XcInspector is a macOS app, and command line tool,
to scan Xcode project files for possible security issues.

- [Source Code Repository on GitLab](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity)
- [List of planned checks (rules)](https://gitlab.com/SwiftDevCollective/XcodeProjectSecurity/-/blob/main/Checks.md?)

## Why?

There are some ways that Xcode projects can be used to compromise security and
privacy.

- Arbitrary scripts can be embedded in a project,
  which may be set to automatically run when certain actions occur
  (such as the common “Build” command).
- Scripts may reference paths outside the project directory.
- Executable files/scripts/libraries can be included in project archives.

This tool is an effort to try (not guarantee) to catch some known risks with
Xcode projects.

> **Note:** XcInspector will not scan source files (Swift, Objective-C, etc.)
> for any sort of security issues with their code.
> That is a separate concern beyond the scope of this project.

## Installation

XcInspector is available as both an app, and a command line tool, for macOS.
Both are in an “alpha” state—meaning they are usable,
but still kind of clunky and incomplete.

There are not yet downloadable packages, so you will have to use Xcode to
compile and build the tools from the source in this project.

The project and sources are in the `XcInspector` directory.

> We use a sub-directory here because our sample files
> (`TestSamples` directory), used by the unit and UI tests,
> include Xcode project files—including some deliberately malformed files.
> Having those in the project directory would _confuse_ Xcode
> (as we learned the hard way).

### To build the App

* Open the project in Xcode.
* Select the “XcInspectorApp” scheme
  (Product Menu -> Scheme -> XcInspectorApp).
* Product Menu -> Build
* Find the compiled application deep in the DerivedData hierarchy:
  * Inside your Xcode DerivedData directory
    (usually at `~/Library/Developer/Xcode/DerivedData`
  * There should be a directory starting with `XcodeProjectSecurity`,
    followed by a pile of seemingly random characters.
  * Inside that directory, navigate to `Build/Products/Debug`
  * There you should find (among many other things) `XcInspectorApp.app`
  * Copy that to your Applications, Utilities, or whichever other directory
    you want to put it.

### To build the Command Line Tool

* Open the project in Xcode.
* Select the “CommandLineToolRelease” scheme
  (Product Menu -> Scheme -> CommandLineToolRelease).
* Product Menu -> Build
* Find the compiled tool deep in the DerivedData hierarchy:
  * Inside your Xcode DerivedData directory
    (usually at `~/Library/Developer/Xcode/DerivedData`
  * There should be a directory starting with `XcodeProjectSecurity`,
    followed by a pile of seemingly random characters.
  * Inside that directory, navigate to `Build/Products/Release`
  * There you should find (among many other things) an executable
    `xcinspector` file.
  * Copy that to whichever directory you want to put it in
    (e.g., `/usr/local/bin`).

## Usage

### XcInspector App

To scan an Xcode project, select the “Open…” command (⌘-O) from the “File”
menu in the app. Then select a directory with an Xcode project file
(`.xcodeproj`) in it, and tap the “Open” button.

A report window will come up telling you how many, if any, possible issues
were found. The report window will also list each issue identified, including
the specific file, and (if applicable) which line number the issue was
encountered at.

### xcinspector Command Line Tool

Make sure the command line tool is somewhere you can access it
(such as `/usr/local/bin`).

To get instructions on the options available when running the tool,
use the command: `xcinspector --help`

In general, you can get a report for a given Xcode project by passing the path
to the project directory, or the project file (.xcodeproj) itself.
E.g.: `xcinspector /User/me/Projects/MyApp`

The resulting report will be output to the terminal (`stdout`), and is in the
Xcode warnings format (`filepath:linenumber:(error|warning|note):message`).
If no issues were found, the report will be empty (only a blank line will be
output).

You can optionally include a summary at the end of the report (which will be
shown even if no issues were found).
`xcinspector --stats path/to/project` or `xcinspector -s path/to/project`

You can also optionally include extended details for any issues found.
(Currently, this only shows the text of the script when an embedded script is
found.)
`xcinspector --details path/to/project` or `xcinspector -d path/to/project`

## Technology Used

- macOS 14 Ventura (or later)
- Xcode 15 (or later)
- SwiftUI
- [SwiftLint](https://github.com/realm/SwiftLint)

---

## Legal

### DISCLAIMER

While this tool may help identify some possible security risks,
it can IN NO WAY GUARANTEE the safety of any particular Xcode project,
or any derivative product.

By using this software, you agree that you are solely responsible for ensuring
the security of any Xcode projects you use or share, and absolve all authors
of, contributors to, or distributors of, XcInspector of any responsibility or
liability.

### Copyright

Copyright ©2023-2024 Grant Neufeld, et.al.

### License

The Source Code of this project is made available under the terms of the
[Mozilla Public License, v. 2.0](https://mozilla.org/MPL/2.0/).

The [license](LICENSE) file is included with the project files.
